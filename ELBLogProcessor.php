<?php
/**
 * Script to process a directory of ELB Logs and search for 5XX errors 
 */
$dir = 'ELBLogs';

$summary = [];

foreach (scandir($dir) as $file) {
    if ($file === '.' || $file === '..') {
        continue;
    }

    $fh = fopen($dir . '/' . $file, 'r');
    while ($line = fgetcsv($fh, 0, ' ')) {
        if (count($line) < 25) {
            continue;
        }

        list($protocol,
            $timestamp,
            $elb,
            $client,
            $target,
            $requestProcessingTime,
            $targetProcessingTime,
            $responseProcessingTime,
            $elbStatusCode,
            $targetStatusCode,
            $recievedBytes,
            $sentBytes,
            $request,
            $userAgent,
            $sslCipher,
            $sslProtocol,
            $targetGroupArn,
            $traceId,
            $domainName,
            $chosenCertArn,
            $matchedRulePriority,
            $requestCreationTime,
            $actionsExecuted,
            $redirectUrl,
            $errorReason)  = $line;
        
        list($requestMethod, $requestUrl) = explode(' ', $request);
        $requestUrlParts = parse_url($requestUrl);
        if (!isset($requestUrlParts['scheme']) || !isset($requestUrlParts['host']) || !isset($requestUrlParts['path'])) {
            continue;
        }
        $parsedRequestUrl = $requestUrlParts['scheme'] . '://' . $requestUrlParts['host'] . @$requestUrlParts['path'];
        $parsedRequestUrl = preg_replace('/[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}/', '{UUID}', $parsedRequestUrl);

        if ($elbStatusCode >= 500 && $targetStatusCode === '-') {
            $timestamp = date("Y-m-d H:i:s", strtotime($timestamp));
            //echo implode(' ', $line);
            //echo "$client $target\n"; continue;
            echo sprintf(
                "[%s][%d][%4d, %4d] %7s %s\n",
                $timestamp,
                $elbStatusCode,
                $recievedBytes, $sentBytes,
                $requestMethod, $requestUrl
            );
            @$summary[$elbStatusCode][$requestMethod][$parsedRequestUrl] += 1;
        }
    }
}

print_r($summary);
