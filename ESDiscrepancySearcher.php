<?php
/**
 * Quick, dirty, and slightly flakey script to scan the media manager elasticsearch instance for data discrepancies
 * compared to the database.
 *
 * DB creds are grabbed from environment variables... so set them first! (DB_USER and DB_PASS)
 *
 * Script will output a line of json for every discrepancy into discrepancies.txt as well echoing progress to stdout
 */
ini_set('memory_limit', '1G');
date_default_timezone_set('Australia/Sydney');

class ESDiscrepancySearcher
{
    /**
     * @var DateTime Start of current time window
     */
    protected $dateStart;

    /**
     * @var DateTime End of current time window
     */
    protected $dateEnd;

    /**
     * @var int Current offset within the current time window
     */
    protected $offset;

    /**
     * @var int Maximum number to process in one batch
     */
    protected $batchSize = 5000;

    /**
     * @var int Total number of videos checked
     */
    protected $checked = 0;

    /**
     * @var int Total number of videos found in ES but not in DB
     */
    protected $missing = 0;

    /**
     * @var int Total number of videos with suppressed mismatches
     */
    protected $suppressedMismatch = 0;

    /**
     * @var int Total number of videos with unsuppressed mismatches
     */
    protected $mismatch = 0;

    /**
     * @var string[] Array of mismatch counts keyed by field name
     */
    protected $mismatchesByKey = [];

    /**
     * @var string[] Array of fields to suppress mismatches for. Enter them manually, I lazily haven't provided an option for it
     */
    protected $suppress = [ ];

    /**
     * @var PDO DB Connection
     */
    protected $db;

    /**
     * ESDiscrepancySearcher constructor: Create DB connection and initialise
     */
    public function __construct()
    {
        $this->db = new PDO(
            'mysql:dbname=switch_content;host=db-master.prod.switch.internal',
            getenv('DB_USER'),
            getenv('DB_PASS')
        );

        $this->dateStart = new DateTime('first day of this week midnight');
        $this->dateEnd   = new DateTime('last day of this week midnight');
        $this->offset    = 0;
    }

    /**
     * Main entry point. Run the things
     * @throws Exception
     */
    public function run()
    {
        do {
            do {
                $assets = $this->getAssetsFromES();
                $this->offset += $this->batchSize;

                foreach ($assets as $asset) {
                    $this->processAsset($asset['_source']);
                }

                arsort($this->mismatchesByKey);

                echo "======================================================\n";
                echo "= Checked:             " . $this->checked . "\n";
                echo "= Suppressed mismatch: " . $this->suppressedMismatch . "\n";
                echo "= Other mismatch:      " . $this->mismatch . "\n";
                echo "= Missing from DB:     " . $this->missing . "\n";
                echo "------------------------------------------------------\n";
                foreach ($this->mismatchesByKey as $key => $count) {
                    echo "= $key: $count \n";
                }
                echo "======================================================\n";


            } while (!empty($assets));

            // Reset, move back in time a week and startagain
            $this->offset = 0;
            $this->dateStart = $this->dateStart->sub(new DateInterval('P1W'));
            $this->dateEnd = $this->dateEnd->sub(new DateInterval('P1W'));

            echo "Resetting date range to " . $this->dateStart->format("Y-m-d") . " TO " . $this->dateEnd->format("Y-m-d") . "\n";
        } while (!empty($assets) || ($this->offset == 0));
    }

    /**
     * Fetch a single asset from the DB and compare it the elasticsearch results
     * @param array $esAsset Key value array of fields from elasticsearch
     */
    protected function processAsset(array $esAsset)
    {
        $id = (int) $esAsset['videoID'];

        if (!$id) {
            echo "No Id found.. something aint right\n";
            return;
        }

        $dbAsset = $this->db->query("select * from switch_content.videos where videoId=" . $id)
                            ->fetch(PDO::FETCH_ASSOC);

        if (!$dbAsset) {
            echo "[$id] ASSET MISSING FROM DB\n";
            $this->missing++;
            return;
        }

        $suppressedMismatch = false;
        $mismatches = [];
        foreach ($esAsset as $key=>$esValue) {

            // ES has some fields that don't map directly to videos table; ignore that for now
            if (!array_key_exists($key, $dbAsset)) {
                continue;
            }

            $dbValue = $dbAsset[$key];

            // DB likes to return strings, ES likes to return all the things. Cast ES numerics to string
            if (is_numeric($esValue)) {
                $esValue = (string) $esValue;
            }

            if ($esValue !== $dbValue) {
                // Lazy mans badly initialised count array
                @$this->mismatchesByKey[$key]++;

                // Optionally suppress requested fields
                if (in_array($key, $this->suppress)) {
                    $suppressedMismatch = true;
                    continue;
                }

                $mismatches[$key] = [ 'es' => $esValue, 'db' => $dbValue ];
            }
        }

        $this->checked++;

        if ($suppressedMismatch) {
            $this->suppressedMismatch++;
        }

        if (!empty($mismatches)) {
            $this->mismatch++;
            foreach ($mismatches as $key => $mismatch) {
                echo "[$id] $key :: " . $this->truncate($mismatch['es']) . ' !== ' . $this->truncate($mismatch['db']) . "\n";
                file_put_contents('discrepancies.txt', json_encode([
                    'id' => $id,
                    'field' => $key,
                    'db' => $mismatch['db'],
                    'es' => $mismatch['es']
                ]) . "\n", FILE_APPEND);
            }
        }
    }

    /**
     * Truncate a value to max 50 chars and append ...
     *
     * @return string
     * **/
    protected function truncate($str) {
        $limit = 50;
        if (strlen($str) < $limit) {
            return $str;
        }

        return substr($str,0, $limit) . '...';

    }

    /**
     * Fetch up to $this->batchSize assets from ES for the time window specified by $this->dateStart to $this->dateEnd
     * and beginning form $this->offset
     *
     * @return array
     * @throws Exception Array of asset results
     */
    protected function getAssetsFromES()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, '10.2.3.49/switch_content/videos/_search');
        curl_setopt($ch, CURLOPT_PORT, 9200);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
            'from' => $this->offset,
            'size' => $this->batchSize,
            'query' => [
                'range' => [
                    'created' => [
                        'gte' => $this->dateStart->format('Y-m-d H:i:s'),
                        'lte' => $this->dateEnd->format('Y-m-d H:i:s')
                    ]
                ]
            ]
        ]));
        curl_setopt($ch, CURLOPT_ENCODING, "");

        $result = curl_exec($ch);

        if (!$result || !json_decode($result) || curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
            echo $result;
            throw new Exception("Failed to get assets from elasticsearch");
        }

        return json_decode($result, true)['hits']['hits'];
    }

}

(new ESDiscrepancySearcher())->run();
