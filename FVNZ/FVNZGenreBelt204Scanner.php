<?php
foreach (file('genreBelt204s') as $line) {
    list($time, $url) = explode(',', trim($line), 2);

    $parts = parse_url($url);

    foreach (explode('&', $parts['query']) as $param) {
        @list($key, $value) = explode('=', $param, 2);
        $params[$key] = $value;
    }

    $time = strtotime($time);
    $client_time = strtotime(urldecode($params['client_time']));

    if ($client_time < $time) {
        echo date(DATE_ATOM, $time) . "\t" . date(DATE_ATOM, $client_time) . "\t" . $url . "\n";
    }
}