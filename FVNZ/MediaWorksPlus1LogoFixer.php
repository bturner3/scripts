<?php
$urlTemplate = "http://fvnz-capi-prod.switch.tv/content/v1/%s/%s";
$auth = "hlEKTBaEzTfE8yIUQnCYGRYeERDoDWx2ImnjxJM9";

$replace = [
    'channel_id/TV3Plus1' => 'channel_id/TV3',
    'channel_number/8' => 'channel_number/3',
    'channel_number_dth/8' => 'channel_number_dth/3',
    'channel_id/BravoPlus1' => 'channel_id/Bravo',
    'channel_number/9' => 'channel_number/4',
    'channel_number_dth/9' => 'channel_number_dth/4'
];

foreach (file('mediaWorksPlus1LogoFixerInput.txt') as $entity) {
    list($type, $id, $theRest) = explode(',', $entity, 3);

    $url = sprintf($urlTemplate, $type . 's', $id);
    try {
        $categories = get($url)['data']['categories'];
    } catch (Exception $e) {
        echo "Failed to GET $url :: " . $e->getMessage() . "\n";
        continue;
    }

    $newCategories = explode('||', str_replace(array_keys($replace), array_values($replace), implode('||', $categories)));
    $payload = [ 'data' => [ 'categories' => $newCategories ] ];

    try {
        echo "Updating $type $id categories to: " . implode(',', $newCategories) . "\n";
        patch($url, $payload);
    } catch (Exception $e) {
        echo "Failed to update $url :: " . $e->getMessage() . "\n";
    }
}

function get($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($status !== 200) {
        throw new Exception("$status: $result");
    }

    return json_decode($result, true);
}

function patch($url, $payload)
{
    global $auth;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Content-Type: application/json",
        "Authorization: Bearer $auth"
    ]);

    $result = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($status !== 200) {
        throw new Exception("$status: $result");
    }

    return json_decode($result, true);
}
