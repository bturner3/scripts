<!--
quite possibly the worlds quickest, dirtiest, mini TV guide thingy.
don't judge too harshly. If it proves useful it will get better over time :D

run: php -S localhost:3000
visit: http://localhost:3000/miniguide.php
-->

<form>
    Start: <input type="datetime-local" name="start" />
    End: <input type="datetime-local" name="end" />
    <input type="checkbox" name="gaps-only" /> Gaps Only
    <input type="submit" value="submit" />
</form>

<?php

ini_set('memory_limit', -1);
if (!$_GET['start'] || !$_GET['end']) {
    return;
}

$channels = curl("http://fvnz-capi-prod.switch.tv/content/v1/channels?sort=lcn&include_related=1&related_entity_types=images&expand_related=full&limit=100");

foreach ($channels as $channel) {
    if (isset($seen[$channel['dvb_triplet']])) {
        continue;
    }
    $seen[$channel['dvb_triplet']] = 1;

    $start = urlencode(date(DATE_ATOM, strtotime(urldecode($_GET['start']))));
    $end = urlencode(date(DATE_ATOM, strtotime(urldecode($_GET['end']))));
    $lastEnd = null;

    $triplet = $channel['dvb_triplet'];
    $epgs = getEpgs($triplet, $start, $end);

    if (empty($epgs)) {
        $colour = '#F08080';
        $msg = "NO EPGS FOUND";
    } else {
        $gaps = 0;
        $colour = '#90EE90';
        $lastEnd = null;
        foreach ($epgs as $epg) {
            if ($lastEnd && $lastEnd !== $epg['start']) {
                $gaps++;
                $colour = '#F08080';
            }
            $lastEnd = $epg['end'];
        }

        $first = date("d M H:i", strtotime($epgs[0]['start']));
        $last = date("d M H:i", strtotime($epgs[count($epgs) - 1]['end']));
        $msg = "$first - $last <br /> " . count($epgs) . " epgs / $gaps gaps";
    }

    if (isset($_GET['gaps-only']) && $gaps == 0) {
        continue;
    }
    echo "<div style='width:500000px'>";
    echo "<span style='display:inline-block;width:175px;height:50px;background-color:$colour;float:left'>
        {$channel['channel_name']} <br />
        <span style='font-size:small'>$msg</span>
    </span>";

    $lastEnd = null;
    foreach ($epgs as $epg) {
        if ($lastEnd && $lastEnd !== $epg['start']) {
            $gapDuration = (strtotime($epg['start']) - strtotime($lastEnd)) / 60;
            $gapWidth = $gapDuration * 5;
            $dateFormat = isset($_GET['gaps-only']) ? 'd M H:i' : 'H-i';
            echo "<span style='display:inline-block;width:{$gapWidth}px;height:50px;border:1px solid red;background-color:lightcoral;float:left'>" .
                date($dateFormat, strtotime($lastEnd)) . " - " . date($dateFormat, strtotime($epg['start'])) .
            "</span>";
        }

        if (!isset($_GET['gaps-only'])) {
            [$hours, $minutes] = explode(':', $epg['duration']);
            $width = ($hours * 60 + $minutes) * 5;
            echo "<span style='display:inline-block;width:{$width}px;height:50px;border:1px solid green;float:left'>" .
                date('H:i', strtotime($epg['start'])) . " - " . date('H:i', strtotime($epg['end'])) .
                "</span>";
        }
        $lastEnd = $epg['end'];
    }

    echo "</div>";
    echo "<div style='clear:both' />";
    echo "<br /><br />";
}

function curl($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($status === 204) {
         return [];
    }

    $json = json_decode($response, true);
    return $json['data'];
}

function getEpgs($triplet, $start, $end)
{
    $offset = 0;
    $allEpgs = [];
    do {
        $newEpgs = curl("http://fvnz-capi-prod.switch.tv/content/v1/epgs/$triplet?limit=100&offset=$offset&sort=start&start=$start&end=$end");

        $allEpgs = array_merge($allEpgs, $newEpgs);
        $offset += 100;
    } while (!empty($newEpgs));

    return $allEpgs;
}
