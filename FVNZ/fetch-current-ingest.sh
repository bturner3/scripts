#!/bin/bash

ENV=$1
PROFILE="switch-$ENV"
BUCKET="freeview-nz-epg-$ENV"
LISTING_DIR="epg_listings"

echo "Using credential profile $PROFILE, working with AWS bucket $BUCKET"

echo "Downloading state file"
AWS_PROFILE=$PROFILE aws s3 cp s3://$BUCKET/.state . --quiet
LAST_DIR=`tail -n1 .state | cut -d : -f 1`

echo "Downloading contents of last directory processed ($LAST_DIR)"
mkdir -p $LAST_DIR
AWS_PROFILE=$PROFILE aws s3 sync s3://$BUCKET/$LAST_DIR/ $LAST_DIR --quiet


# Deleting local state file
rm .state

