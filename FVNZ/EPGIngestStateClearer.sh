#!/bin/bash
# Script to clear the state from EPG Ingest in order to force files from a given point onwards
# to be re-preocessed.
#
# The following example would clear all state from 3:15 am october 1st 2019 onwards:
# ./EPGIngestStateClearer.sh prod 2019100100315

if [ ! $2 ]; then
  echo "Usage: $0 [stage|sandbox|prod] [Dir to clear from]"
  echo "EG: $0 prod 201910010310"
  exit 1
fi

ENV=$1
START_DATE=$2
PROFILE="switch-$ENV"
BUCKET="freeview-nz-epg-$ENV"
LISTING_DIR="epg_listings"
DIR="epg-state-cleaner"

echo "Using credential profile $PROFILE, working with AWS bucket $BUCKET"

# Create working directory
mkdir -p $DIR

# Download state file
AWS_PROFILE=$PROFILE aws s3 cp s3://$BUCKET/.state $DIR/

# Initialise new state file
echo -n "" > $DIR/newState

# Scan state file and clear state files that match
for LINE in `cat "$DIR/.state"`; do
  DATE=${LINE%:*}

  if [[ $DATE > $START_DATE ]]; then
    AWS_PROFILE=$PROFILE aws s3 rm s3://$BUCKET/$DATE/.state

    echo -n "\n$DATE:0" >> $DIR/newState
  else
    echo $LINE >> $DIR/newState
  fi;
done;


AWS_PROFILE=$PROFILE aws s3 cp $DIR/newState s3://$BUCKET/.state

# Clean up
rm -rf $DIR