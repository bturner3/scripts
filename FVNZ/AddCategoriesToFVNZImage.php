<?php declare(strict_types = 1);

/*
 * Quick and dirty script demonstrating how to update the categories on an image
 * based on it's UUID and a list of categories to append.
 *
 * Sample usage: php AddCategoriesToFVNZImage.php 52bb5955-8dab-458e-a37e-9164d8403230 dth_dvb_triplet:002F:0016:040C
 */

if ($argc !== 3) {
    echo "ERROR: Unexpected number of arguments\n";
    echo "Usage: " . $argv[0] . "image-uuid \"CSV,of,categories,to add\"\n\n";
    exit(1);
}

// Boring set up crap.
$id = $argv[1];
$add_categories = $argv[2];
$api_base = "http://fvnz-capi-stage.switch.tv/content/v1/images/";

// Get image info.
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $api_base . $id);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if (!$result || !json_decode($result) || $status !== 200) {
    echo "ERROR: Failed to fetch image details for image $id\n";
    echo "Status code: $status, Message: $result\n";
    exit(1);
}
$image = json_decode($result, true);

// Merge current categories with ones to add.
$current_categories = $image['data']['categories'];
$new_categories = array_unique(array_merge(
    $current_categories,
    explode(',', $add_categories)
));

// Perform update.
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $api_base . $id);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode([ 'data' => [ 'categories' => $new_categories ] ]));
curl_setopt($curl, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'authorization: Bearer EB581D40EE7A29FE74F61A95A70AA24E1A1860E0',
]);
$result = curl_exec($curl);
$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if (!$result || $status !== 200) {
    echo "ERROR: Image update failed\n";
    echo "Status code: $status, Message: $result\n";
    exit(1);
}

$result = json_decode($result, true);
echo "Image updated successfully. New categories are: " . print_r($result['data']['categories'], true);
