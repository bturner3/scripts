<?php
// Script to copy channel logo images from one environment to another
$options = getopt(
    'l',
    [ 'live', 'clearTargetFirst', 'source:', 'target:' ]
);

$copier = new ChannelImageLogoCopier();

try {
    if (isset($options['l']) || isset($options['live'])) {
        $copier->setLive(true);
    }

    if (isset($options['clearTargetFirst'])) {
        $copier->setClearTarget(true);
    }

    if (isset($options['source'])) {
        $copier->setSourceEnv($options['source']);
    }

    if (isset($options['target'])) {
        if (!is_array($options['target'])) {
            $options['target'] = [$options['target']];
        }

        foreach ($options['target'] as $target) {
            $copier->addTargetEnv($target);
        }
    }

    $copier->run();
} catch (Exception $e) {
    echo "ERROR: " . $e->getMessage() . "\n";
}

class ChannelImageLogoCopier
{
    const ENV_TYPE_SOURCE = 'source';
    const ENV_TYPE_TARGET = 'target';

    const ENV_STAGE = 'stage';
    const ENV_SANDBOX = 'sandbox';
    const ENV_PROD = 'prod';

    /** @var bool Whether or not the script is running in live mode to perform actual changes */
    protected bool $live = false;

    /** @var bool If set to true any channel logo images that currently exist on the target site are cleared first */
    protected bool $clearTarget = false;

    /** @var string[] Array of target environments to which logos should be copied */
    protected array $targetEnvs = [];

    /** @var ?string The source environment from which logos will be copied */
    protected ?string $sourceEnv = null;


    /**
     * @param bool $live
     * @return $this
     */
    public function setLive(bool $live): self
    {
        $this->live = $live;
        return $this;
    }

    /**
     * @param bool $clearTarget
     * @return $this
     */
    public function setClearTarget(bool $clearTarget): self
    {
        $this->clearTarget = $clearTarget;
        return $this;
    }


    /**
     * @param string $env
     * @return $this
     * @throws Exception
     */
    public function setSourceEnv(string $env): self
    {
        $this->validateEnv($env);

        $this->sourceEnv = $env;
        return $this;
    }

    /**
     * @param string $env
     * @return $this
     * @throws Exception
     */
    public function addTargetEnv(string $env): self
    {
        $this->validateEnv($env);
        $this->targetEnvs[] = $env;
        $this->targetEnvs = array_unique($this->targetEnvs);

        return $this;
    }

    /**
     * Main processing entry point
     *
     * @throws Exception
     */
    public function run()
    {
        $this->validateOptions();
        $this->checkEnv();

        $sourceImages = $this->getChannelLogos($this->sourceEnv);

        foreach ($this->targetEnvs as $targetEnv) {
            if ($this->clearTarget) {
                $this->clearEnv($targetEnv);
            }

            foreach ($sourceImages as $image) {
                $result = $this->retry(
                    function() use ($image, $targetEnv) { return $this->post($image, $targetEnv); },
                    "Error creating image"
                )['data'];

                echo sprintf(
                    "Created image %s from %s for channel %s\n",
                    $result['id'],
                    $result['url'],
                    $result['categories'][0]
                );
            }
        }
    }

    /**
     * @param string $env
     * @throws Exception
     */
    protected function clearEnv(string $env) 
    {
        $logos = $this->getChannelLogos($env);
        echo "Deleting existing logos from $env\n";
        foreach ($logos as $logo) {
            echo "Deleting image: " . $logo['id'] . "\n";
            try {
                $this->retry(
                    function () use ($logo, $env) {
                        $this->delete($logo['id'], $env, 'fvnz');
                    },
                    "Error deleting image as FVNZ"
                );
            } catch (AuthException $e) {
                try {
                    $this->retry(
                        function () use ($logo, $env) {
                            $this->delete($logo['id'], $env, 'tvnz');
                        },
                        "Error deleting image as TVNZ"
                    );
                } catch (Exception $e) {
                    throw $e;
                }
            } catch (Exception $e) {
                echo "Giving up on deleting image " . $logo['id'] . ": " . $e->getMessage() . "\n";
                continue;
            }
        }
    }

    /** @throws Exception */
    protected function checkEnv()
    {
        foreach ($this->targetEnvs as $env) {
            $this->getAuth($env);
        }
    }

    /**
     * @param string $env
     * @param string $broadcaster
     * @return string
     * @throws Exception
     */
    protected function getAuth(string $env, $broadcaster = 'fvnz')
    {
        $authVar = "FVNZ_" . strtoupper($broadcaster) . "_AUTH_" . strtoupper($env);
        $auth = getenv($authVar);
        if (!$auth) {
            throw new Exception("$authVar not set in env. Unable to authenticate for $env");
        }

        return (string) $auth;
    }

    /**
     * @param string $env
     * @return array
     * @throws Exception
     */
    protected function getChannelLogos(string $env): array
    {
        $offset = 0;
        $logos = [];
        do {
            $url = $this->getBaseUrl($env) .
                "/content/v1/images?tags=channel-logo&categories=channel_id/*&limit=100&offset=$offset";

            list($status, $response) = $this->retry(
                function() use ($url) { return $this->get($url); },
                "Failed to get $url"
            );

            if ($status === 200) {
                $decodedResponse = json_decode($response, true);
                $logos = array_merge($logos, $decodedResponse['data']);
                $offset += 100;
            }
        } while ($status !== 204);

        return $logos;
    }

    /**
     * @param callable $callable
     * @param string $errorMessage
     * @return mixed
     * @throws Exception
     */
    protected function retry(callable $callable, string $errorMessage)
    {
        $attempt = 1;
        $maxAttempts = 5;

        do {
            try {
                return $callable();
            } catch (RetryableException $e) {
                if ($attempt == $maxAttempts) {
                    throw $e;
                }
                $delay = 1 * pow($attempt, 2);
                $attempt++;
                echo "$errorMessage. Retrying in $delay seconds\n";
                echo "\t" . $e->getMessage() . "\n";
                sleep($delay);
            } catch (Exception $e) {
                echo "Non retryable exception: " . $e->getMessage() . "\n";
                throw $e;
            }
        } while (true);
    }

    /**
     * @param $url
     * @return array
     * @throws Exception
     */
    protected function get($url): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status >= 400) {
            throw new RetryableException("Failed to get $url. statsus=$status, response=$response");
        }
        return [ $status, $response ];
    }

    /**
     * @param array $image
     * @param string $targetEnv
     * @return array
     * @throws Exception
     */
    protected function post(array $image, string $targetEnv): array
    {
        $payload = json_encode([ 'data' => [
            'categories' => $image['categories'],
            'draft' => $image['draft'],
            'url' => $image['url'],
            'tags' => $image['tags']
        ] ]);

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $this->getBaseUrl($targetEnv) . "/content/v1/images",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $payload,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $this->getAuth($targetEnv),
                "Content-Type: application/json"
            ),
        ]);

        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status !== 201) {
            throw new RetryableException("Error creating image: " . $response);
        }

        return json_decode($response, true);
    }

    /**
     * @param string $imageId
     * @param string $targetEnv
     * @param string $broadcaster
     * @return bool
     * @throws Exception
     */
    protected function delete(string $imageId, string $targetEnv, string $broadcaster): bool
    {
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $this->getBaseUrl($targetEnv) . "/content/v1/images/$imageId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $this->getAuth($targetEnv, $broadcaster),
                "Content-Type: application/json"
            ),
        ]);

        curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status === 404) {
            return true;
        } elseif ($status === 403) {
            throw new AuthException("Error deleting image: status=$status");
        } elseif ($status !== 200) {
            throw new RetryableException("Error deleting image: status=$status");
        }

        return true;
    }



    /**
     * @param string $env
     * @return $this
     */
    protected function getBaseUrl(string $env): string
    {
        return "http://fvnz-capi-$env.switch.tv";
    }

    /**
     * @param string $env
     * @throws Exception
     */
    protected function validateEnv(string $env)
    {
        if (!in_array($env, [ self::ENV_STAGE, self::ENV_SANDBOX, self::ENV_PROD ])) {
            throw new Exception("'$env' is not a valid environment");
        }

    }

    /**
     * @throws Exception
     */
    protected function validateOptions()
    {
        if ($this->sourceEnv === null) {
            throw new Exception('Source env has not been set. Use setSourceEnv() to do so');
        }

        if (count($this->targetEnvs) === 0) {
            throw new Exception('No target envs have been set. Use addTargetEnv() to do so');
        }

        if (in_array($this->sourceEnv, $this->targetEnvs)) {
            throw new Exception('Source env is included in target envs. This is nonsensical');
        }
    }
}


class RetryableException extends Exception {}
class AuthException extends Exception {}