<?php
/**
 * Create tsv dump of current FVNZ cognito users. Relies on an AWS profiled named switch-prod to exist with permission to read from cognito in prod
 */
$nextToken = null;
$outFile = 'cognitoUserExport.tsv';
$userCount = 0;
file_put_contents($outFile, "Username\temail\tsub\tstatus\tcreate date\tupdate date\tenabled\n");

do {
    $cmd = "AWS_PROFILE=switch-prod aws cognito-idp list-users --user-pool-id=ap-southeast-2_fFCJUmhO4 --max-items=5000";
    if ($nextToken !== null) {
        $cmd .= " --starting-token=$nextToken";
    }

    $out = null;
    exec($cmd, $out);
    $output = json_decode(implode('', $out), true);

    foreach ($output['Users'] as $user) {
        $attributes = [];
        foreach ($user['Attributes'] as $rawAttribute) {
            $attributes[$rawAttribute['Name']] = $rawAttribute['Value'];
        }

        file_put_contents($outFile, $user['Username'] . "\t" .
            $attributes['email'] . "\t" .
            $attributes['sub'] . "\t" .
            $user['UserStatus'] . "\t" .
            $user['UserCreateDate'] . "\t" .
            $user['UserLastModifiedDate'] . "\t" .
            ($user['Enabled'] ? "True" : "False") . "\n", FILE_APPEND);
        $userCount++;
    }

    echo "Completed: $userCount, last ID: {$user['Username']}\n";
    $nextToken = $output['NextToken'];
} while (!empty($output['Users']) && !empty($nextToken));
