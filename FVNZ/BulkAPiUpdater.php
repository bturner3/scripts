<?php

if (empty($argv[1])) {
    echo "No id file or type specified\n";
    exit(1);
}
if (empty($argv[2])) {
    echo "No type specified. should be episodes or shows\n";
    exit(1);
}
if (empty($argv[3]) || !in_array($argv[3], [ 'stage', 'sandbox', 'prod'  ])) {
    echo "No, or invalid, env provided\n";
    exit(1);
}

$idFile = $argv[1];
$type = $argv[2];
$env = $argv[3];
$token = $env == 'prod' ? 'lWNHrTvkUpDrAoWLbjYCRgecNG9Urvs3QN98phpg' : '8A85A28773D18CB33DAD57A5C3E1580FE5791A9B';

if (!file_exists($idFile) || (!is_readable($idFile))) {
    echo "Non existeent or unreadable file: $idFile\n";
    exit(1);
}

$curl = curl_init();
curl_setopt_array($curl, [
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'PATCH'
]);

$ids = array_map('trim', file($idFile));

$chunks = array_chunk($ids, 20);
foreach ($chunks as $chunk) {
    $handles = [];
    foreach ($chunk as $id) {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PATCH',
            CURLOPT_URL => "http://fvnz-capi-{$env}.switch.tv/content/v1/$type/$id",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Authorization: Bearer $token"
            ),
        ]);

        if ($type === 'shows') {
            $payload = '{ "data": { "on_demand": false } }';
        } else {
            $payload = '{ "data": { "available_to": "2021-06-01T00:00:00+00:00" } }';
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);

        $handles[] = $curl;
    }

    $multi = curl_multi_init();
    foreach ($handles as $handle) {
        curl_multi_add_handle($multi, $handle);
    }

    $running = null;
    do {
        curl_multi_exec($multi, $running);
    } while ($running);

    foreach ($handles as $handle) {
        curl_multi_remove_handle($multi, $handle);
    }
    curl_multi_close($multi);

    foreach ($handles as $handle) {
        echo curl_getinfo($handle, CURLINFO_EFFECTIVE_URL) . ": " . curl_getinfo($handle, CURLINFO_HTTP_CODE) . "\n";
    }
}

curl_close($curl);

