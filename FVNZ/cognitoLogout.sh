#!/bin/bash

# Script to remotely log out a user from cognito based on their email.
# It is possible for multiple user accounts to exist for a given email
# in which case all matching accounts will be logged out
#
# Usage: ./cognitoLogout.sh EMAIL AWS_PROFILE
#
# Pre requisites: If not available you will be prompted to install
#   - AWS cli: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
#   - jq: https://stedolan.github.io/jq/download/
#
# Running this script also requires AWS credentials to be configured
# in a profile in ~/.aws/credentials
#
# A sample credentials file using the default profile name should look like:
#
#   [switch]
#   aws_access_key_id = <AWS_ACCESS_KEY_ID>
#   aws_secret_access_key = <AWS_SECRET_ACCESS_KEY>
#
#   [switch-prod]
#   source_profile = switch
#   role_arn = arn:aws:iam::744362489876:role/OrganizationAccountAccessRole
#   mfa_serial = arn:aws:iam::<AWS_ACCOUNT_ID>:mfa/<IAM_USER_NAME>
#   region = ap-southeast-2
#
# The expected values of the placeholders above can be found in the AWS console:
# <AWS_ACCOUNT_ID>: click on your email in the top right of the console and copy the Account ID (exclude -'s)
# <IAM_USER_NAME>: Copy the IAM user value from the same dropdown
# <AWS_ACCESS_KEY_ID> and <AWS_SECRET_ACCESS_KEY> can be managed from https://us-east-1.console.aws.amazon.com/iam/home?region=us-east-1#/
#     under the "Access Keys for CLI, SDK, & API access" section

if [ ! "$1" ] || [ "$3" ]; then
  echo "Usage: $0 EMAIL_TO_LOG_OUT [AWS_CREDENTIALS_PROFILE_NAME]"
  echo "Eg: $0 turner@switch.tv switch-prod"
  exit 1
fi

DEPENDENCIES_MET=true
if ! command -v aws &> /dev/null; then
  echo "Required aws command not found."
  echo "Follow the instructions at https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html to install it"
  DEPENDENCIES_MET=false
fi

if ! command -v jq &> /dev/null; then
  echo "Required jq command not found."
  echo "Follow the instructions at https://stedolan.github.io/jq/download/ to install it"
  DEPENDENCIES_MET=false
fi

$DEPENDENCIES_MET || exit 1

EMAIL=$1
USER_POOL="ap-southeast-2_fFCJUmhO4"
PROFILE=${2:-switch-prod}

USERS=$(AWS_PROFILE=$PROFILE aws cognito-idp list-users --user-pool-id=$USER_POOL --filter="email = '$EMAIL'")
USER_NAMES=$(echo "$USERS" | jq -r '.Users[].Username')

for USER_NAME in $USER_NAMES; do
  echo "Logging out user id: $USER_NAME"

  # Note: Don't put anything between the following 2 lines
  AWS_PROFILE=$PROFILE aws cognito-idp admin-user-global-sign-out --user-pool-id=$USER_POOL --username="$USER_NAME"
  EXIT_CODE=$?
  # Note: Don't put anything between the previous 2 lines

  if [[ $EXIT_CODE -eq 0 ]]; then
    echo "Success"
  else
    echo "Failed with exit code $EXIT_CODE";
  fi
done;