<?php
require 'aws.phar';

ini_set('memory_limit', '-1');
(new EPGGapReconciler())->run();

class EPGGapReconciler
{
    /** @var string  */
    protected $env;

    /** @var \Aws\S3\S3Client  */
    protected $s3;

    /** @var array  */
    protected $channels = [];

    protected $accountIds = [
        'stage' => '948618524387',
        'sandbox' => '830847678586',
        'prod' => '744362489876'
    ];

    /**
     * @param string $env
     */
    public function __construct($env = 'stage')
    {
        $this->env = $env;
        $this->bucket = 'freeview-nz-epg-' . $env;
        $this->s3 = $this->getS3Client();
    }

    public function run()
    {
        file_put_contents('failedIngestions', '');

        echo "Loading CAPI channels...";
        $this->loadCapiChannels();
        echo " Done: Loaded " . count($this->channels) . "channels\n";

        echo "Downloading state file... ";
        $stateFile = $this->getFromS3('.state')['Body'];
        $dir = $this->getLastDirProcessed($stateFile);
        echo "Done\n";

        if (!is_dir($dir)) {
            mkdir($dir);
        }

        echo "Downloading contents of latest directory '$dir'";
        $this->s3->downloadBucket($dir, $this->bucket, "$dir/");
        echo "Done\n";

        $files = scandir($dir);
        foreach ($files as $file) {
            if (substr($file, -4) !== '.xml') {
                continue;
            }

            echo "Processing $file...\n";
            $this->processFile($dir . '/' . $file);
            echo "Done\n\n";
        }

        exec("rm -rf $dir");
    }

    /**
     * @param $file
     * @throws Exception
     */
    protected function processFile($file)
    {
        $xml = simplexml_load_file($file);

        $triplet = (string) $xml->attributes()->DVBTriplet;
        if (!$triplet) {
            echo "DVB Triplet not found in XML";
            return;
        }

        $channel = $this->channels[$triplet] ?? null;
        if (!$channel) {
            echo "\tDid not find CAPI channel for DVB Triplet '$triplet'\n";
            return;
        }

        echo "\tLoading entries from XML... ";
        $xmlEPGS = $this->getEPGSFromXML($xml);
        if (empty($xmlEPGS)) {
            echo "FAILED: No entries found\n";
        } else {
            $first = $this->formatDate($xmlEPGS[0]['start']);
            $last = $this->formatDate($xmlEPGS[count($xmlEPGS) - 1]['end']);
            echo "Done: Loaded " . count($xmlEPGS) . " entries spanning from $first - $last\n";
        }

        echo "\tLoading CAPI epgs for channel $triplet... ";
        $capiEPGS = $this->getEPGSForChannel($triplet);
        if (empty($capiEPGS)) {
            echo "FAILED: No epgs found in CAPI\n";
            return;
        } else {
            $first = $this->formatDate($capiEPGS[0]['start']);
            $last = $this->formatDate($capiEPGS[count($capiEPGS) - 1]['end']);
            echo "Done: Loaded " . count($capiEPGS) . " epgs spanning from $first - $last\n";
        }

        foreach ($capiEPGS as $epg) {
            if (isset($lastEnd) && $lastEnd != $epg['start']) {
                echo sprintf("\tGap detected from %s to %s\n", $this->formatDate($lastEnd), $this->formatDate($epg['start']));

                foreach ($xmlEPGS as $idx => $xmlEpg) {
                    if ($this->isInGap($xmlEpg, $lastEnd, $epg['start'])) {
                        echo sprintf(
                            "\t\tGap Filler found: %s from %s - %s\n\t\t%s\n",
                            $xmlEpg['title'],
                            $this->formatDate($xmlEpg['start']),
                            $this->formatDate($xmlEpg['end']),
                            str_replace("\n", '', $xmlEpg['full'])
                        );

                        file_put_contents('failedIngestions', $xmlEpg['full'] . "\n-------------------------------------------------\n", FILE_APPEND);
                    }
                }
            }

            $lastEnd = $epg['end'];
        }
    }

    protected function isInGap($epg, $gapStart, $gapEnd)
    {
        $epgStart = strtotime($epg['start']);
        $epgEnd = strtotime($epg['end']);
        $gapStart = strtotime($gapStart);
        $gapEnd = strtotime($gapEnd);

        if ($epgStart >= $gapStart && $epgStart < $gapEnd) {
            return true;
        }

        if ($epgEnd > $gapStart && $epgEnd <= $gapEnd) {
            return true;
        }

        return false;
    }

    protected function formatDate($date, $format = 'd M H:i')
    {
        if (!is_numeric($date)) {
            $date = strtotime($date);
        }

        return date($format, $date);
    }

    protected function getEPGSFromXML(SimpleXMLElement $xml)
    {
        $epgs = [];
        foreach ($xml->programmes->programme as $epg) {
            $start = $epg->attributes()->datetime_start;
            $end = $epg->attributes()->datetime_end;
            $title = $epg->title;

            $epgs[] = [ 'start' => $start, 'end' => $end, 'title' => $title, 'full' => $epg->asXML() ];
        }

        return $epgs;
    }

    protected function getEPGSForChannel($triplet)
    {
        $offset = 0;
        $allEpgs = [];

        do {
            $epgResult = $this->curlGateway("epgs/$triplet?limit=100&offset=$offset&sort=start");

            $epgs = $epgResult['data'] ?? [];
            $allEpgs = array_merge($allEpgs, $epgs);
            $more = $epgResult['meta']['pagination']['more'] ?? false;
            $offset += 100;
        } while ($more);

        return $allEpgs;
    }

    protected function loadCapiChannels()
    {
        $channels = $this->curlGateway('channels?limit=100')['data'];

        $channelsByTriplet = [];
        foreach ($channels as $channel) {
            $channelsByTriplet[$channel['dvb_triplet']] = $channel;
        }

        $this->channels = $channelsByTriplet;
    }

    protected function getLastDirProcessed($stateFile)
    {
        $lines = explode("\n", $stateFile);
        $lastLine = end($lines);
        [ $dir, $status ] = explode(':', $lastLine);

        return $dir;
    }

    protected function curlGateway($path)
    {
        $host = sprintf('fvnz-capi-%s.switch.tv', $this->env);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$host}/content/v1/$path");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status === 204) {
            return [];
        }

        return json_decode($result, true);
    }

    /**
     * @param $file
     * @return \Guzzle\Service\Resource\Model
     */
    protected function getFromS3($file)
    {
        return $this->s3->getObject([ 'Bucket' => $this->bucket, 'Key' => $file ]);
    }

    /**
     * @return \Aws\S3\S3Client
     */
    protected function getS3Client()
    {
        // Create STS client
        $stsClient = new Aws\Sts\StsClient([
            'profile' => 'default',
            'region' => 'ap-southeast-2',
            'version' => 'latest'
        ]);

        // Assume role
        $accountId = $this->accountIds[$this->env];
        $credentials = $stsClient->AssumeRole([
            'RoleArn' => "arn:aws:iam::$accountId:role/OrganizationAccountAccessRole",
            'RoleSessionName' => 's3-access'
        ]);

        // Connect to S3 with assumed credentials
        return new \Aws\S3\S3Client([
            'version' => 'latest',
            'region' => 'ap-southeast-2',
            'credentials' => [
                'key'    => $credentials['Credentials']['AccessKeyId'],
                'secret' => $credentials['Credentials']['SecretAccessKey'],
                'token'  => $credentials['Credentials']['SessionToken']
            ]
        ]);
    }
}
