<?php
require_once 'MDRParser.php';

if (!isset($argv[1])) {
    echo "No env specified";
    exit;
}
$env = $argv[1];

$channels = (new MDRParser($env))->generateChannelList();
foreach ($channels as $channel) {
    echo $channel;
    $triplets[] = trim($channel->DVBTriplet);
}

echo "Generated list of " . count($triplets) . " channels to ingest\n";
echo implode(',', $triplets) . "\n";