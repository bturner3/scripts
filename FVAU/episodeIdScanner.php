<?php
/**
<programme genre="Action" subgenre=""  classification="(M)" program_type="series">
    <title>Miami Vice</title>
    <synopsis>Two students get in trouble smuggling cocaine-filled statues into the country for a gangster. (1984)</synopsis>
    <program_id>142723</program_id>
    <connectorId>SH033838480000</connectorId>
    <tmsId>EP033838480011</tmsId>
    <event_id>113022333</event_id>
    <crid crid_type="programme" crid_value="/MIVI_1_59512"></crid>
    <date_time_duration ['date_time_value']="2020-05-11T14:00:00Z" duration="00:57"></date_time_duration>
    <series episode_number="11" season_number="1">
    <title>Miami Vice</title>
    <episode_title>The Milk Run</episode_title>
    <synopsis></synopsis>
    <crid crid_type="series" crid_value="/MIVI"></crid>
    </series><media>
    <filename>1699717_MIVI01.png</filename>
    <url>https://contentserver.com.au/assets/tv/1699717_MIVI01.png</url>
    <timestamp>2020-01-20T13:57:21</timestamp>
    </media>
    <highlight dont_miss="false" coming_up="false" catch_up="false" />
    <blackout>no</blackout>
    <catch_up>
        <link type="HbbTV">dvb://current.ait/145.4001?external-id=/MIVI_1_59512&amp;custom-path=/catchup</link>
        <link type="ios">ninenow://www.9now.com.au/crid/%2fMIVI_1_59512</link>
        <link type="android">ninenow://www.9now.com.au/crid/%2fMIVI_1_59512</link>
    </catch_up>
    <cast>Don Johnson, Philip Michael Thomas, Saundra Santiago</cast>
</programme>
 */
$earliestStart = PHP_INT_MAX;
$noTmsId = 0;
$noConnectorId = 0;
$entries = 0;
foreach (scandir('./listings') as $file) {
    if ($file === '.' || $file === '..') {
        continue;
    }

    $xml = simplexml_load_file('./listings/' . $file);
    $channel = $xml->service['service_name'];
    foreach ($xml->service->schedule->programme as $programme) {
        $title = (string) $programme->title;
        $tmsId = (string) $programme->tmsId;
        $crid = (string) $programme->crid['crid_value'];
        $start = (string) $programme->date_time_duration['date_time_value'];
        $connectorId = (string) $programme->tmsId;
        $imageUrl = (string) $programme->media->url;
        if (strtotime($start) < $earliestStart) {
            $earliestStart = strtotime($start);
        }

        $results[$crid][] = "$imageUrl";
       // echo "$tmsId / $crid - $imageUrl ($channel)\n";
       // echo "[$file @ $start] $title :: $tmsId - $crid\n";
    }
}

foreach ($results as $crid=>$images) {
    echo "$crid " . implode("  ", array_unique($images)) . "\n";
}
