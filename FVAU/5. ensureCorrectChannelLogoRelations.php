<?php
require_once 'MDRParser.php';
$dry = false;
if (!isset($argv[1])) {
    echo "No env specified";
    exit;
}
$env = $argv[1];
$gw = new GatewayClient($env);
$mdr = new MDRParser($env);

// Build map of required logo for each channel
foreach ($mdr->readMDR(false) as $channel) {
    $channelLogos[$channel->formattedTriplet()] = $channel->Logo;
}

// Fetch all channels from CAPI
$channels = $gw->fetchAllChannels('include_related=1&related_entity_types=images&expand_related=full');

// Attach pre defined logos, and detach others
$logosByTag = [];
foreach ($channels as $channel) {
    echo "Checking " . $channel['channel_name'] . ': ' . $channel['dvb_triplet'] . "\n";

    // Determine name of the logo used for this channel
    $logoKey = $channelLogos[$channel['dvb_triplet']] ?? null;
    if ($logoKey === null) {
        echo "SKIPPING: Unable to determine logo for channel {$channel['channel_name']} / {$channel['dvb_triplet']} (triplet does not appear in MDR)\n";
        continue;
    }

    // Determine tag that identifies logos for this channel
    $normalisedTitle = str_replace(' ', '_', $logoKey);
    if (!$normalisedTitle) {
        $normalisedTitle = 'sbs_viceland';
    }
    $logoTag = 'channel-logo-' . $normalisedTitle;
    echo "Using logo tag $logoTag\n";

    // Find existing logos
    $existingLogos = $gw->get('/content/v1/images?tags=' . $logoTag);
    if (!isset($logosByTag[$logoTag])) {
        $logos = [ 'light-bg' => null, 'dark-bg' => null ];
        if (!isset($existingLogos['data'])) {
            continue;
        }
        foreach ($existingLogos['data'] as $logo) {
            if (in_array('channel-logo-light-bg', $logo['tags'])) {
                $logos['light-bg'] = $logo['id'];
            } elseif (in_array('channel-logo-dark-bg', $logo['tags'])) {
                $logos['dark-bg'] = $logo['id'];
            }
        }
        $logosByTag[$logoTag] = $logos;
    }

    $logos = $logosByTag[$logoTag];
    echo "Got " . count($logos) . " existing logos\n";
    if (count($logos) === 0) {
        echo "SKIPPING: Could not found any logos for $logoTag in capi. Run 4. createLogosInCapi.php first\n";
        continue;
    }

    // Detach existing logos
    $existingImages = $channel['related']['images'] ?? [];
    $alreadyAttached = [];
    foreach ($existingImages as $image) {
        if (in_array($image['id'], $logos)) {
            //echo "-- Keeping: {$channel['id']} / {$image['id']}\n";
            $alreadyAttached[$image['id']] = true;
            continue;
        }

        try {
            echo "Detaching: {$channel['id']} / {$image['id']}\n";
            if (!$dry) {
                $gw->delete("/content/v1/channels/{$channel['id']}/images/{$image['id']}");
            }
        } catch (Exception $e) {
            echo "-- Failed: " . $e->getMessage() . "\n";
        }
    }

    // Attach desired logos
    if (!empty($logos['light-bg']) && !isset($alreadyAttached[$logos['light-bg']])) {
        try {
            echo "Attaching: {$channel['id']} / {$logos['light-bg']}\n";
            if (!$dry) {
                $gw->post("/content/v1/channels/{$channel['id']}/images/{$logos['light-bg']}");
            }
        } catch (Exception $e) {
            echo "-- Failed: " . $e->getMessage() . "\n";
        }
    }

    if (!empty($logos['dark-bg']) && !isset($alreadyAttached[$logos['dark-bg']])) {
        try {
            echo "Attaching: {$channel['id']} / {$logos['dark-bg']}\n";
            if (!$dry) {
                $gw->post("/content/v1/channels/{$channel['id']}/images/{$logos['dark-bg']}");
            }
        } catch (Exception $e) {
            echo "-- Failed: " . $e->getMessage() . "\n";
        }
    }
}

// Scan through logo images and delete any that are not related to anything
/*
$offset = 0;
$imagesToDelete = [];
do {
    $result = $gw->get("/content/v1/images?limit=100&offset=$offset&include_related=1&tags=channel-logo");

    foreach ($result['data'] as $image) {
        if (empty($image['related'])) {
            $imagesToDelete[] = $image['id'];
        }
    }
    $offset += 100;
} while ($result['meta']['pagination']['more'] === true);

foreach ($imagesToDelete as $image) {
    try {
        echo "Deleting image: $image\n";
        if (!$dry) {
            $gw->deleteImage($image);
        }
    } catch (Exception $e) {
        echo "-- Failed: " . $e->getMessage() . "\n";
    }
}
*/