<?php
require_once 'MDRParser.php';

$parser = new MDRParser(MDRParser::ENV_PROD);
foreach ($parser->readMDR() as $channel) {
    echo $channel;
}
/*
$file = fopen('assets/regionMapping.csv', 'r');
$states = [];
while ($line = fgetcsv($file)) {
    $states[$line[1]] = $line[0];
}
foreach ($parser->readMDR() as $channel) {
    $region = $channel->Region;
    $state = $states[$region];

    $regions[$state][$region] = $region;
}


foreach ($regions as $state => $stateRegions) {
    echo "$state\n";
    foreach ($stateRegions as $region) {
        echo "\t$region\n";
    }
}
*/