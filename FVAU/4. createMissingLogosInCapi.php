<?php
require_once 'MDRParser.php';
if (!isset($argv[1])) {
    echo "No env specified";
    exit;
}
$env = $argv[1];

$originBase = "http://img-store-prod.switch.tv/freeview-au-channel-logos";
$logoFile = fopen('assets/logos', 'r');
$refresh = true;
$columns = fgetcsv($logoFile);

$gw = new GatewayClient($env);

// Determine required logos
$logoTitles = [];
foreach ((new MDRParser())->generateChannelList() as $channel) {
    $logoTitles[] = $channel->Logo;
}
$logoTitles = array_values(array_unique($logoTitles));

// Collect logo file names
$logoFile = fopen('assets/logos', 'r');
while ($line = fgetcsv($logoFile)) {
    $fileNames[$line[0]] = $line[1];
}

foreach ($logoTitles as $logoTitle) {
    // Normalise and determine file name of local logo
    $normalisedTitle = str_replace(' ', '_', $logoTitle);
    if (!$normalisedTitle) {
        $normalisedTitle = 'sbs_viceland';
    }
    $fileName = $fileNames[$logoTitle] ?? null;
    if (!$fileName) {
        echo "Could not determine file name for logo: $logoTitle\n";
        continue;
    }

    // Find existing logos
    $existingLogos = $gw->get('/content/v1/images?tags=channel-logo-' . $normalisedTitle);
    if ($refresh && !empty($existingLogos)) {
        foreach ($existingLogos['data'] as $logo) {
            echo "Deleting existing logo: {$logo['id']}\n";
            $gw->deleteImage($logo['id']);
        }
        $existingLogos = [];
    }

    $lightId = $darkId = null;
    foreach ($existingLogos['data'] ?? [] as $logo) {
        if (in_array('channel-logo-light-bg', $logo['tags'])) {
            //echo "Found existing light-bg logo for $normalisedTitle\n";
            $lightId = $logo['id'];
        } elseif (in_array('channel-logo-dark-bg', $logo['tags'])) {
            //echo "Found existing dark-bg logo for $normalisedTitle\n";
            $darkId = $logo['id'];
        }
    }

    // Create any that weren't found
    if (!$lightId) {
        try {
            $lightImage = $gw->createImage([
                'url' => "$originBase/light-bg/$fileName",
                'tags' => [
                    'channel-logo',
                    'channel-logo-dark',
                    'channel-logo-light-bg',
                    'channel-logo-' . $normalisedTitle
                ]
            ])['data'];
            echo "Created new light-bg logo for $normalisedTitle -- {$lightImage['id']}\n";
        } catch (Exception $e) {
            echo "Failed to create light-bg image for $logoTitle: " . $e->getMessage() . "\n";
        }
    }


    if (!$darkId) {
        try {
            $darkImage = $gw->createImage([
                'url' => "$originBase/dark-bg/$fileName",
                'tags' => [
                    'channel-logo',
                    'channel-logo-dark-bg',
                    'channel-logo-' . $normalisedTitle
                ]
            ])['data'];
            echo "Created new dark-bg logo for $normalisedTitle -- {$darkImage['id']}\n";
        } catch (Exception $e) {
            echo "Failed to create dark-bg image for $logoTitle: " . $e->getMessage() . "\n";
            print_r($line);
        }
    }
}
