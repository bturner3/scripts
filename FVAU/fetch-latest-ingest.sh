#!/bin/bash

# Script to fetch the latest ingestion folder from S3 and drop it into the local directory ready to be processed
# This script relies on AWS credential profiles being set up in a specific way in order to work.
# Specifically it is expected that the following profiles exist and are able to authenticate to the relevant account
# - switch-stage
# - switch-sandbox
# - switch-prod

set -euo pipefail

[ $# = 0 ] && echo "Usage: $0 (stage|sandbox|prod)" && exit 1;

ENV=$1;
PROFILE="switch-$ENV"
BUCKET="freeview-au-epg-$ENV"

set +e
echo "Downloading global state file"
AWS_PROFILE=$PROFILE aws s3 cp s3://$BUCKET/.state . --quiet
if [ $? = 1 ]; then
    echo "Failed to download state file. This probably means you don't have AWS credentials configured as expected"
    echo "A $PROFILE profile is expected to be configured in ~/.aws/credentials" 
    exit 1;
fi
set -e

LATEST_DIR=`tail -n1 .state | cut -d : -f 1`

echo "Downloading $LATEST_DIR to ./listings/"
rm -rf listings
AWS_PROFILE=$PROFILE aws s3 sync s3://$BUCKET/$LATEST_DIR/ listings --quiet

rm -rf .state
