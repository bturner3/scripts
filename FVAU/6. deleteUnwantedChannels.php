<?php
require_once 'MDRParser.php';
if (!isset($argv[1])) {
    echo "No env specified";
    exit;
}
$env = $argv[1];

$mdr = new MDRParser($env);
$gw = new GatewayClient($env);
$activeChannels = $mdr->generateChannelList();
$capiChannels = $gw->loadChannelIds();

foreach ($activeChannels as $channel) {
    $triplet = implode(':', str_split($channel->DVBTriplet, 4));

    $channelsToKeep[$triplet] = true;
}
$seen = [];
foreach ($capiChannels as $triplet => $id) {
    if (isset($channelsToKeep[$triplet]) && !isset($seen[$triplet])) {
        $seen[$triplet] = true;
        echo "KEEP: $triplet / $id\n";
        continue;
    }


    echo "DELETE: $triplet / $id\n";
    $gw->deleteChannel($id);
}
