<?php
class MDREntry {
    public $DVBTriplet;
    public $LCN;
    public $ServiceType;
    public $ServiceName;
    public $NetworkName;
    public $Region;
    public $DefaultAuthority;
    public $Ghost;
    public $LiveStream;
    public $Member;
    public $Logo;
    public $CatchUpPortalURL;

    public function __construct($line)
    {
        foreach ($line as $key => $value) {
            $key = str_replace(' ', '', $key);

            if ($key == 'DVBTriplet') {
                $value = str_replace([ '0x', '.' ], [ '', '' ], $value);
            }
            $this->{$key} = $value;
        }
    }

    public function __toString()
    {
        return sprintf(
            "[%3s:%-20s][LCN: %-3d] %-45s\t %s / %-26s \tGhost:%-3s Streaming:%s\n",
            $this->Division(),
            $this->Region,
            $this->LCN,
            $this->NetworkName . '/' . $this->ServiceName,
            $this->formattedTriplet(),
            $this->DefaultAuthority,
            $this->Ghost,
            $this->LiveStream
        );
    }

    public function isTV()
    {
        return $this->ServiceType === 'TV';
    }

    public function isGhost()
    {
        return $this->Ghost === 'yes';
    }

    public function formattedTriplet()
    {
        return trim(implode(':', str_split($this->DVBTriplet, 4)));
    }

    public function Division()
    {
        $file = fopen('assets/regionMapping.csv', 'r');

        $fields = fgetcsv($file);
        while ($row = fgetcsv($file)) {
            $row = array_combine($fields, $row);

            if ($row['region'] == $this->Region) {
                return $row['division'];
            }
        }
    }
}
class MDRParser {
    const NATIONAL_TAG = 'region_national';
    const CURRENT_PROD_CHANNELS = '101002110211,101002210220,101002210221,101002210222,101002210223,101002210224,101002310231,101002410241,101002510251,101002710271,101002810281,101204000401,101204000406,101204000407,101204000408,101204000409,10120400040A,10120400040B,101204000480,101204200421,101204200424,101204200425,101204200427,101204200428,101204300430,101204300433,101204300434,101204300436,101204300437,101204500451,101204500454,101204500455,101204500457,101204500458,101204600461,101204600464,101204600465,101204600467,101204600468,101305020520,101305020522,101305020523,101305020525,101305020527,101305020528,101305030530,101305030532,101305030533,101305030535,101305030537,101305040540,101305040542,101305040543,101305040545,101305040547,101305040557,101305040567,101305050550,101305050552,101305050553,101305050555,101305060560,101305060562,101305060563,101305060565,101305070570,101309000910,101309000912,101309000913,101309000915,101309100920,101309100922,101309100923,101309200930,101309200932,101309200933,101309300940,101309300942,101309300943,101309400950,101309400952,101309400953,101309500960,101309500962,101309500963,101309600970,101309600972,101309600973,101406020621,101406020625,101406020628,101406030631,101406030635,101406030638,101406040641,101406040645,101406040648,101406050651,101406050655,101406050658,101406070681,101406070685,101406070688,3201026102E1,320203000301,320203000302,320203000303,320203000304,320203000305,320203000307';
    const CURRENT_NATIONAL_CHANNELS = '101002210222,101002210224,101305020528,320203000302,320203000305,101002210220,101204000406,101204000480,320203000301,320203000304';

    /* STAGE */
    const DIVISIONS_TO_ENABLE = [ /* 'VIC' */ ];
    const OUT_OF_DIVISION_REGIONS = [ 'Melbourne', 'Albury/Wodonga', 'Regional VIC' /*'Sydney', 'Adelaide', 'Brisbane', 'Hobart', 'Perth', 'Darwin' */ ];

    // SANDBOX
    //const DIVISIONS_TO_ENABLE = [ 'VIC' ];
    //const OUT_OF_DIVISION_REGIONS = [ /*'Sydney', 'Adelaide', 'Brisbane', 'Hobart', 'Perth', 'Darwin' */ ];


    const ENV_STAGE = 'stage';
    const ENV_SANDBOX = 'sandbox';
    const ENV_PROD = 'prod';

    /**
     * @var string
     */
    protected $env;

    /**
     * @var array Region info fetched from CAPI regions_without_postcodes document
     */
    protected $regions = [];

    /**
     * MDRParser constructor.
     *
     * @param $env
     * @throws Exception
     */
    public function __construct($env = self::ENV_STAGE)
    {
        $this->setEnv($env);
        $this->apiClient = new GatewayClient($env);
    }

    /**
     * @param string $env
     * @throws Exception
     */
    public function setEnv(string $env): void
    {
        if (!in_array($env, [ self::ENV_PROD, self::ENV_SANDBOX, self::ENV_STAGE ])) {
            throw new Exception(
                sprintf(
                    "Invalid environment '$env', Must be one of %s, %s, or %s",
                    self::ENV_STAGE, self::ENV_SANDBOX, self::ENV_PROD
                )
            );
        }

        $this->env = $env;
    }

    /**
     * Inefficient way to work... but convenient
     * Read all valid channels into an array of MDREntry objects
     *
     * @return MDREntry[]
     */
    public function readMDR($filtered = true)
    {
        $file = fopen('assets/MDR.csv', 'r');
        $fields = fgetcsv($file);
        $mdr = [];
        while ($line = fgetcsv($file)) {
            $channel = new MDREntry(array_combine($fields, $line));
            // Skip non TV channels, and ghost channels
            if ($filtered && (!$channel->iSTV() || $channel->isGhost())) {
                continue;
            }

            $mdr[] = $channel;
        }

        return $mdr;
    }

    /**
     * @return array of regional channel names based on the DVB triplets of the channels currently flagged as national
     *  (just in case our names don't match up)
     */
    protected function getNamesOfNationalChannels(): array
    {
        $national = [];
        foreach ($this->readMDR() as $channel) {
            if (in_array($channel->DVBTriplet, explode(',', self::CURRENT_NATIONAL_CHANNELS))) {
                $national[] = $channel->ServiceName;
            }
        }

        return $national;
    }

    /**
     * @return array key value array of channel counts by region
     */
    public function countChannelsByRegion(): array
    {
        $channelCountsByRegion = [];
        foreach ($this->readMDR() as $channel) {
            if (!isset($channelCountsByRegion[$channel->Region])) {
                $channelCountsByRegion[$channel->Region] = 0;
            }

            $channelCountsByRegion[$channel->Region]++;
        }

        return $channelCountsByRegion;
    }

    /**
     * @param string $region
     * @return MDREntry[] array of channels from the specified region
     */
    public function getChannelsByRegion(string $region): array
    {
        $channels = [];
        foreach ($this->readMDR() as $channel) {
            if ($channel->Region == $region) {
                $channels[] = $channel;
            }
        }

        return $channels;
    }

    /**
     * Initial entry point before we grew big that changes purpose over time!
     * @return MDREntry[]
     */
    public function generateChannelList(): array
    {
        $nationalChannelNames = $this->getNamesOfNationalChannels();

        $prodChannels = explode(',', self::CURRENT_PROD_CHANNELS);
        $includedChannels = [];
        foreach ($this->readMDR() as $channel) {
            // Duplicate prod
            if (in_array($channel->DVBTriplet, $prodChannels)) {
                $includedChannels[] = $channel;
                continue;
            }


            // OR: State level selection
            if (in_array($channel->Division(),self::DIVISIONS_TO_ENABLE)) {
                $includedChannels[] = $channel;
            } else if (in_array($channel->Region, self::OUT_OF_DIVISION_REGIONS)) {
                $includedChannels[] = $channel;
            }


            // OR: For now, keep the current list of national channels and ignore anything else with the same names
            /*
            if (in_array($channel->ServiceName, $nationalChannelNames)) {
                if (in_array($channel->DVBTriplet, explode(',', self::CURRENT_NATIONAL_CHANNELS))) {
                    $includedChannels[] = $channel;
                }
                continue;
            }

            // Include all remaining channels from our desired regions
            if (in_array($channel->Region, self::REGIONS_TO_ENABLE)) {
                $includedChannels[] = $channel;
            }
            */
        }

        return $includedChannels;
    }

    public function loadRegions(): array
    {
        $data = fopen('assets/regionMapping.csv', 'r');
        $fields = fgetcsv($data);
        while ($line = fgetcsv($data)) {
            $regions[] = array_combine($fields, $line);
        }

        $this->regions = $regions;
        return $regions;
    }

    /**
     * @param string $targetRegion
     * @return ?string
     */
    public function getTagForRegion(string $targetRegion): ?string
    {
        if (empty($this->regions)) {
            $this->loadRegions();
        }

        foreach ($this->regions as $regionData) {
            if ($targetRegion == $regionData['region']) {
                return $regionData['tag'];
            }
        }

        return null;
    }
}

class GatewayClient
{
    public function __construct(string $env = MDRParser::ENV_STAGE)
    {
        $this->env = $env;
    }

    /**
     * @return string
     */
    protected function capiHost()
    {
        return "http://fvau-api-{$this->env}.switch.tv";
    }

    /**
     * @return array
     * @throws Exception
     */
    public function fetchDocument($slug): array
    {
        return $this->get("/content/v1/documents/$slug")['data'];
    }

    public function generateUpdateDocumentRequest($id, $patch): string
    {
        $data = json_encode([ 'data' => $patch ]);
        $host = "http://api.capi.{$this->env}";
        return "curl -X PATCH \\\n" .
            "$host/v1/documents/{$id} \\\n" .
            "-H 'Content-Type: application/json' \\\n" .
            " -d '$data'";
    }

    public function fetchAllChannels($params = null)
    {
        $offset = 0;
        $limit = 100;
        $channels = [];

        do {
            $result = $this->get("/content/v1/channels?limit=$limit&offset=$offset&$params");
            $channels = array_merge($channels, $result['data']);

            $offset += $limit;
        } while ($result['meta']['pagination']['more']);

        return $channels;
    }

    /**
     * Calls the channels endpoint on the FVAU gateway and returns a list of channels
     * @return array
     * @throws Exception
     */
    public function loadChannelIds()
    {
        $offset = 0;
        $limit = 100;
        $channelIds = [];

        do {
            $result = $this->get("/content/v1/channels?limit=$limit&offset=$offset");
            foreach ($result['data'] as $channel) {
                $channelIds[$channel['dvb_triplet']] = $channel['id'];
            }

            $offset += $limit;
        } while ($result['meta']['pagination']['more']);

        return $channelIds;
    }

    /**
     * @param string $id
     * @return array
     * @throws Exception
     */
    public function getChannel(string $id): array
    {
        $result = $this->get("/content/v1/channels/{$id}");

        if (!isset($result['data'])) {
            throw new Exception("Failed to fetch channel data");
        }

        return $result['data'];
    }

    public function createChannel(array $data)
    {
        return $this->post('/content/v1/channels/', json_encode([ 'data' => $data ]));
    }
    /**
     * @param string $id
     * @param array  $patch
     * @throws Exception
     */
    public function updateChannel(string $id, array $patch)
    {
        $url = "/content/v1/channels/{$id}";

        $payload = [ 'data' => $patch ];

        $this->patch($url, json_encode($payload));
    }

    /**
     * @param string $id
     */
    public function deleteChannel(string $id)
    {
        $this->delete("/content/v1/channels/{$id}");
    }

    /**
     * @param $url
     * @return array
     * @throws Exception
     */
    public function delete($url): array
    {
        $auth = getenv('FVAU_AUTH_' . strtoupper($this->env));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->capiHost() . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                "Authorization: Bearer $auth",
                'Content-Type: application/json',
            ]
        );
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status !== 200 && $status !== 404) {
            throw new Exception("$status - " . $result);
        }

        return json_decode($result, true);
    }

    /**
     * @param $url
     * @param $payload
     * @return array
     * @throws Exception
     */
    protected function patch($url, $payload): array
    {
        $auth = getenv('FVAU_AUTH_' . strtoupper($this->env));
        if (!$auth) {
            throw new Exception("Auth not found");
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->capiHost() . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                "Authorization: Bearer $auth",
                'Content-Type: application/json',
            ]
        );
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (!$result || $status !== 200) {
            throw new Exception("$status: $result");
        }

        return json_decode($result, true);
    }

    public function createImage($data) {
        return $this->post("/content/v1/images", json_encode([ 'data' => $data ]));
    }
    public function deleteImage($id) {
        return $this->delete("/content/v1/images/$id");
    }

    public function post($uri, $payload = null): array
    {
        $auth = getenv('FVAU_AUTH_' . strtoupper($this->env));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->capiHost() . $uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        if ($payload) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        }

        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                "Authorization: Bearer $auth",
                'Content-Type: application/json',
            ]
        );
        $result = json_decode(curl_exec($ch), true);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (!$result || ($status !== 201 && $status !== 200 && $status !== 409)) {
            throw new Exception("$status: " . json_encode($result));
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function get($url): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->capiHost() . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch), true);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status != 200 && $status != 204) {
            throw new Exception("$status: " . json_encode($result));
        }

        return $result ?? [];
    }
}
