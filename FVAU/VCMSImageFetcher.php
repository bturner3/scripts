<?php
$file = fopen('assets/vcmsLogos.csv', 'r');

$fields = fgetcsv($file);

$dir = "/Users/bturner/Downloads/VCMS Logos";
while ($row = fgetcsv($file)) {
    $row = array_combine($fields, $row);

    $file_name = strtolower(str_replace([' ', '/'], '_', $row['service_name'])) . '.png';

    if (file_exists("$dir/$file_name")) {
        continue;
    }

    echo "Downloading {$row['logo_url']} to {$file_name}\n";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $row['logo_url']);
    curl_setopt($ch, CURLOPT_FILE, fopen("$dir/tmp.png", 'w'));
    curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($status !== 200) {
        unlink("$dir/tmp.png");
        file_put_contents('failures', $file_name . "\n" , FILE_APPEND);
        continue;
    }

    rename("$dir/tmp.png", "$dir/$file_name");
    file_put_contents('success', $file_name . "\n", FILE_APPEND);
}