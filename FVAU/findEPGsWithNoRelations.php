<?php

if (empty($argv[1]) || !in_array($argv[1], [ 'stage', 'sandbox', 'prod' ])) {
    exit("Usage: $argv[0] (stage|sandbox|prod)\n");
}

$env = $argv[1];
(new Scanner($env))->run();

class Scanner
{
    protected $env = 'stage';

    public function __construct($env)
    {
        $this->env = $env;
        $this->baseUri = "http://fvau-api-{$env}.switch.tv";
    }

    public function run()
    {
        $channelOffset = 0;
        do {
            $channelResult = $this->curl('/content/v1/channels/region/region_nsw_sydney?limit=100&offset=' . $channelOffset);
            $missingRelated = 0;
            $tba = 0;
            $total = 0;

            foreach ($channelResult['data'] as $channel) {

                $region = implode(',', preg_grep('/region_/', $channel['tags'] ?? []));
                if (empty($region)) {
                    continue;
                }

                $triplet = $channel['dvb_triplet'];
                $offset = 0;
                $limit = 100;

                do {
                    $epgResult = $this->curl("/content/v1/epgs/$triplet?sort=start&include_related=1&related_entity_types=episodes,shows&limit=$limit&offset=$offset");
                    if (empty($epgResult['data'])) {
                        continue;
                    }
                    foreach ($epgResult['data'] as $epg) {
                        $total++;
                        if (isset($epg['related'])) {
                            continue;
                        }

                        if (empty($epg['title'])) {
                            continue;
                        }

                        echo sprintf(
                            "[%s][%s in %s][%s - %s] EPG Id: %s title: %s)\n",
                            $channel['dvb_triplet'],
                            $channel['channel_name'],
                            $region,
                            date('Y-m-d H:i', strtotime($epg['start']) + 36000),
                            date('Y-m-d H:i', strtotime($epg['end']) + 36000),
                            $epg['id'],
                            $epg['title']
                        );

                        $missingRelated++;

                        if ($epg['title'] === "To Be Advised") {
                            $tba++;
                        }
                    }
                    $offset += $limit;
                } while ($epgResult['meta']['pagination']['more'] ?? false);
            }

            $channelOffset += 100;
        } while ($channelResult['meta']['pagination']['more']);

        echo "Total missing: $missingRelated (out of $total checked), $tba are 'To Be Advised' entries\n";
    }


    protected function curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUri . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        return json_decode($result, true);
    }
}







