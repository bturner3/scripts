<?php
require_once 'MDRParser.php';
if (!isset($argv[1])) {
    echo "No env specified";
    exit;
}
$env = $argv[1];
$parser = new MDRParser($env);
$gw = new GatewayClient($env);
$idMap = $gw->loadChannelIds();

foreach ($parser->generateChannelList() as $channel) {
    $triplet = implode(':', str_split($channel->DVBTriplet, 4));
    $tag = $parser->getTagForRegion($channel->Region);
    if (!$tag) {
        echo "$triplet: Tag for region {$channel->Region} not found\n";
        continue;
    }

    try {
        $addTags = [ $tag ];
        if (in_array($channel->DVBTriplet, explode(',', $parser::CURRENT_NATIONAL_CHANNELS))) {
            $addTags[] = $parser::NATIONAL_TAG;
        }

        // Channel with this triplet doesn't exist, create it
        if (!isset($idMap[$triplet])) {
            echo "Creating: " . trim($channel) . " ...";
            try {
                $gw->createChannel([
                    'broadcaster_code' => $channel->NetworkName,
                    'channel_code' => $channel->ServiceName,
                    'channel_name' => $channel->ServiceName,
                    'dvb_triplet' => $channel->formattedTriplet(),
                    'lcn' => $channel->LCN,
                    'tags' => $addTags
                ]);
                echo "Done\n";
            } catch (Exception $e) {
                echo "Failed - " . $e->getMessage() . "\n";
            }
            continue;
        }

        // Otherwise update existing channel
        $id = $idMap[$triplet];
        $capiChannel = $gw->getChannel($id);
        $currentTags = $capiChannel['tags'] ?? [];
        $currentLCN = $capiChannel['lcn'] ?? null;
        $lcn = $channel->LCN;

        if (array_diff($addTags, $currentTags) || $lcn != $currentLCN) {
            echo "Adding LCN=$lcn, tags=" . implode(', ', $addTags) . " to channel $id ... ";

            $gw->updateChannel(
                $id,
                [
                    'lcn' => $lcn,
                    'tags' => array_unique(array_merge($currentTags, $addTags))
                ]
            );
            echo "done!\n";
        }
    } catch (Exception $e) {
        echo $e->getMessage() . "\n";
    }
}

