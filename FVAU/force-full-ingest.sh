#!/bin/bash
set -euo pipefail

[ $# = 0 ] && echo "Usage: $0 (stage|sandbox|prod)" && exit 1;

ENV=$1
PROFILE="switch-$ENV"
BUCKET="freeview-au-epg-$ENV"
LISTING_DIR="epg_listings"

echo "Using credential profile $PROFILE, working with AWS bucket $BUCKET"

echo "Downloading state file"
AWS_PROFILE=$PROFILE aws s3 cp s3://$BUCKET/.state . --quiet
LAST_DIR=`tail -n1 .state | cut -d : -f 1`

echo "Downloading contents of last directory processed ($LAST_DIR)"
mkdir -p $LAST_DIR
AWS_PROFILE=$PROFILE aws s3 sync s3://$BUCKET/$LAST_DIR/ $LAST_DIR --quiet
NEXT_DIR=`echo $LAST_DIR - 1 | bc`

echo "Creating dir for empty files ($NEXT_DIR)"
mkdir -p $NEXT_DIR

echo "Generating files with no listings"
for file in `ls $LAST_DIR`; do
    #if [ $file = "listings_320203000307.xml" ]; then
        head -n4 $LAST_DIR/$file > $NEXT_DIR/$file 
        echo "</schedule></service></listings>" >> $NEXT_DIR/$file 
    #else 
    #    cp $LAST_DIR/$file $NEXT_DIR/
    #fi 
done

echo "Deleting state files from last dir"
AWS_PROFILE=$PROFILE aws s3 rm s3://$BUCKET/$LAST_DIR/.state

echo "Pushing emptied files back up to s3"
AWS_PROFILE=$PROFILE aws s3 sync $NEXT_DIR s3://$BUCKET/$NEXT_DIR 

echo "Deleting last dir from main state file"
cat .state | grep -v $LAST_DIR > .tmp && mv .tmp .state 

echo "Pushing main state file back to s3"
AWS_PROFILE=$PROFILE aws s3 cp .state s3://$BUCKET/.state --quiet

echo "Cleaning up"
rm -rf $LAST_DIR
rm -rf $NEXT_DIR
rm .state

echo "Done. Next ingest file processed will be fully ingested"
