<?php
require_once('MDRParser.php');
$env = $argv[1] ?? 'stage';
$gw = new GatewayClient($env);

$channels = $gw->get('/content/v1/channels?tags=region_nsw_canberra');

foreach ($channels['data'] as $channel) {
    $id = $channel['id'];
    $tags = $channel['tags'];

    $newTags = array_unique(array_merge($tags, [ 'region_act_canberra' ]));

    if ($tags == $newTags) {
        echo "[$id] Nothing to do\n";
        continue;
    }

    echo "[$id] Updating from " . implode(',', $tags) . " to " . implode(',', $newTags) . "\n";
    try {
        $gw->updateChannel($id, ['tags' => $newTags]);
        echo "\tDone\n";
    } catch (Exception $e) {
        echo "\tFAILED: " . $e->getMessage() . "\n";
    }
}