<?php
require_once 'MDRParser.php';
$env = $argv[1] ?? 'stage';
$parser = new MDRParser($env);
$gw = new GatewayClient($env);

$allRegionData = $parser->loadRegions();

$channels = $parser->generateChannelList();
$includedRegions = [];
foreach ($channels as $channel) {
    $includedRegions[] = $channel->Region;
}

$docRegions = [];
foreach ($allRegionData as $regionData) {
    if (in_array($regionData['region'], $includedRegions)) {
        $docRegions[] = $regionData;
    }
}

// Append the national
/*
$docRegions[] = [
    'division' => 'None',
    'region' => 'Nation wide',
    'tag' => 'region_national',
    'timezone' => 'Australia/Sydney'
];
*/

usort($docRegions, function($a, $b) {
    $stateOrder = [ 'None', 'ACT', 'NSW', 'VIC', 'QLD', 'SA', 'WA', 'NT', 'TAS' ];
    $priorityRegions = [ 'placeholder', 'Sydney', 'Melbourne', 'Brisbane', 'Adelaide', 'Perth', 'Darwin' ];

    $aRank = array_search($a['division'], $stateOrder);
    $bRank = array_search($b['division'], $stateOrder);


    // Same state, sort alphabetically by region
    if ($aRank === $bRank) {
        // Same state, check priory regions
        $aRank = array_search($a['region'], $priorityRegions) ?: -1;
        $bRank = array_search($b['region'], $priorityRegions) ?: -1;

        return $aRank < $bRank;
    }

    // Otherwise sort by state priority order
    return ($aRank > $bRank);
});

$document = $gw->fetchDocument('regions_without_postcodes');
echo $gw->generateUpdateDocumentRequest($document['id'], [ 'body' => $docRegions ]) . "\n";