# FVAU scripts


## 1. generateTripletList
Generates a list of DVB triplets from the MDR file based on the following criteria
 
 - TV Channel
 - supported = yes
 - ghost != yes
 
There is also a list of current national channels in the MDR. These will eventually 
be removed in favour of correctly ingesting all regional variations but for now
we ignore all other channels with the same name as the listed national channels
and the same channel is assumed to be used nation wide

## 2. createAndUpdateRequiredChannels
Creates, or updates, all channels identified by as being required for ingestion in CAPI.

This script does not currently include setting up streams in the attributes but it does handle

- broadcaster_code
- channel_code
- channel_name
- dvb_triplet
- lcn
- tags

## 3. generateRegionsWithoutPostcodesDoc
This script generates a CURL command that can be copied and run in the relevant gateway
to update the regions_without_postcodes document with all the regions currently identified
for ingestion.

## 4. createMissingLogosInCapi
Based on a new logo tagging convention of expecting all channel logos to be tagged with `channel-logo-[logo-name]`
this script identifies any required logos that do not currently exist in CAPI and creates them

## 5. ensureCorrectChannelLogoRelations
This script will go through all existing channels in CAPI and attach the logos created
by the script above, as well as detaching all other related images.

**WARNING:** This script is NOT SAFE to run if non logo images are ever attached to channels, however this is currently not a problem

## 6. deleteUnwantedChannels
This goes through and soft deletes any channels that exist in CAPI, but are not in the ingestion list generated in step 1


# Pending stuff / Upcoming steps
- The `regions_with_postcodes` doc is out of date but can not be properly updated until FVAU provide an updated list of region postcode associations
- The scripts above do not add `streams` attributes to channels
- Need to get to the point where we can remove national channels, and ingest all regional variations for all channels