<?php
$url = $argv[1];
//$url = "https://genesis-api-stage.switch.tv/content/v1/documents?realm_id=2e0a53b8-0194-11e8-ba89-0ed5f89f718b&limit=100&slug=on-demand-homepage";
//$url = "https://fvau-web-stage.switch.tv/?";

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$down = null;
$upCount = 0;
while (true) {
    curl_setopt($ch, CURLOPT_URL, $url . "&cache_bust=" . microtime(true));
    $result = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);


    // If it goes down, mark the time
    if (!$down && $status !== 200) {
        echo "\n\e[31mDown at " . date("Y-m-d H:i:s") . "\n";
        $down = microtime(true);
    }

    // If it comes back up, report total down time
    if ($down && $status === 200) {
        echo "\e[32mBack at " . date("Y-m-d H:i:s") . " --  DOWN FOR: " . round(microtime(true) - $down, 2) . " seconds\n";
        $down = false;
        $upCount = 0;
    }

    if ($status === 200) {
        echo "\e[32m.";
        $upCount++;
        if ($upCount == 120) {
            $upCount = 0;
            echo "\n";
        }
    }

    sleep(1);
}
