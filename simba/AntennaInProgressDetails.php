<?php
date_default_timezone_set('Australia/Sydney');
kubectl('get pod', $output, $responseCode);
$output = preg_grep('/mediahq-ingest-vod/', $output);

echo sprintf("| %4s | %-19s | %-50s | %-7s | %-8s |\n", "Pod", "Start time", "File Name", "Size", "Elapsed");
echo "+------+---------------------+" . str_repeat('-', 52) . "+---------+----------+\n";

foreach ($output as $line) {
    list($pod, $junk) = explode(' ', $line);
    list($a,$b,$podLabel,$junk) = explode('-', $pod);
    kubectl("logs $pod --tail=10", $output, $responseCode);
    print_r($output);
    $output = preg_grep('/Processing remote file/', $output);
    print_r($output);
    $inProgress = array_pop($output);
    preg_match('/\[(?<startTime>[^]]+)\] ingest\.INFO: Processing remote file (?<file>[^ ]+)/', $inProgress, $matches);
    $startTime = $matches['startTime'];
    $file = $matches['file'];

    rclone("lsjson s3-antenna-ingest:$file", $output, $responseCode);
    $fileInfo = @json_decode(implode('', $output), true)[0];

    if ($fileInfo) {
        echo sprintf(
            "| %4s | %19s | %-50s | %7s | %8s |\n",
            $podLabel,
            date("Y-m-d H:i:s", strtotime($startTime)),
            $fileInfo['Name'],
            formatBytes($fileInfo['Size']),
            gmdate("H:i:s", (time() - strtotime($startTime)))
        );
    }
}

function kubectl(string $cmd, ?array &$output, ?int &$responseCode)
{
    echo "kubectl --context=antenna-production {$cmd} 2>&1\n";
    $output = [];
    exec("kubectl --context=antenna-staging {$cmd} 2>&1", $output, $responseCode);
}

function rclone(string $cmd, ?array &$output, ?int &$responseCode)
{
    $output = [];
    exec("rclone {$cmd} 2>&1", $output, $responseCode);
}

function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

