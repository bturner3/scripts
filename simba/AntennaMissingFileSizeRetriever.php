<?php
$db = new mysqli(
    '127.0.0.1',
    'root',
    'JMfJRJxGaxfajg3b',
    'switch_content',
    '7001'
);

$sql = "SELECT v.siteId, v.videoId, kv.keyId as kvId, encId.value as encodingId
FROM videos v
JOIN switch_key_values.integer encId on (encId.videoID=v.videoID and encId.keyID = (SELECT keyID from switch_key_values.types where siteId=v.siteId AND name='ENCODING_ASSET_ID'))
LEFT JOIN switch_key_values.string kv on (kv.videoID
=v.videoID and kv.keyID = (SELECT keyID FROM switch_key_values.types WHERE siteId=v.siteId AND name='EST_FILE_SIZE'))
WHERE v.siteID in (353, 354) AND state != 'PREPARE' AND external_ref in ('TV', 'MOVIE') AND (kv.value is null OR kv.value = '' OR kv.value = '0');
";
$results = $db->query($sql);

exec("rclone lsf antenna-prod-origin:1167027/356 --max-depth=1", $inAkamai);
$inAkamai = array_map(function($item) { return trim($item, '/'); }, $inAkamai);
echo "Found " . $results->num_rows . " items to update\n";
foreach ($results->fetch_all(MYSQLI_ASSOC) as $row) {
    $siteId = $row['siteId'];
    $videoId = $row['videoId'];
    $encodingId = $row['encodingId'];
    $keyId = $row['kvId'];
    if (!in_array($encodingId, $inAkamai)) {
        echo "Skipping $encodingId - not in akamai\n";
        continue;
    }

    $cmd = "rclone size antenna-prod-origin:1167027/356/{$encodingId}/H264_1080P 2>&1";
    echo "[" . date("H:i:s") . "] $cmd\n";
    $output = [];
    exec($cmd, $output, $status);

    if (!isset($output[1]) || !preg_match("/Total size: ([^\(]+)/", $output[1], $matches)) {
        echo "\tSize not found\n";
        print_r($output);
        continue;
    }

    $num = round(floatval($matches[1]), 2);
    $suffix = stristr($matches[1], "GB") ? "GB" : "MB";
    $size = "{$num}{$suffix}";
    if ($num == 0) {
        echo "\tSize is 0 - skipping\n";
        continue;
    }

    $sql = "update switch_key_values.string set value='{$size}' where videoId={$videoId} and keyId={$keyId} limit 1";
    echo "[" . date("H:i:s") . "] {$sql}\n";
    $res = $db->query($sql);
}