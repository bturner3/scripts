<?php

$db = new mysqli(
    '127.0.0.1',
    'root',
    'secret',
    'switch_content',
    '7000'
);

$result = $db->query("select * from videos v join site_mapping m on v.videoID=m.videoID");
$affectedCount = 0;
foreach ($result->fetch_all(MYSQLI_ASSOC) as $row) {
    $affected = false;

    foreach ($row as $key=>$value) {
        $decoded = html_entity_decode($value);

        if ($value != html_entity_decode($value)) {
            echo "[" . $row['siteID'] . ':' . $row['videoID'] . "] $value != " . html_entity_decode($value) . "\n";
            $affected = true;
        }
    }

    if ($affected) {
        $affectedCount++;
    }
}

echo "$affectedCount assets affected\n";