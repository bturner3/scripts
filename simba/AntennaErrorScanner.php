<?php
$verbose = isset($argv[1]);

date_default_timezone_set('Australia/Sydney');
$pendingFiles = [];
$folders = [ 'vod', 'vod2', 'vod3', 'vod4', 'vod5', 'vod-priority' ];
$allFiles = [];
$missingMeta = [];
foreach ($folders as $folder) {
    $pending = "s3-antenna-ingest:antennatv-sftp/content/$folder/pending";
    $failed = "s3-antenna-ingest:antennatv-sftp/content/$folder/failed";

    foreach ([ $pending, $failed ] as $dir) {
        rclone("lsjson $dir", $output, $statusCode);
        $files = json_decode(implode('', $output), true);
        $names = [];
        foreach ($files as $file) {
            $names[] = $file['Name'];
        }
        foreach ($files as $file) {
            checkError($file, $pending, $names);
        }
    }
}

function checkError($file, $dir, $names)
{
    global $missingMeta;
    global $verbose;

    $fileName = $file['Name'];
    if (preg_match("/.error.log$/", $fileName)) {
        if (!in_array(str_replace(".error.log", "", $fileName), $names)) {
            #rclone("ls {$dir}/" . str_replace(".error.log", "", $fileName), $output, $responseCode);
            if (empty($output) && $verbose) {
                echo "Orphaned error log file: {$fileName} in {$dir}\n";
                return;
            }
        }

        echo "Failed file: {$fileName} in {$dir}\n";
        rclone("copy {$dir}/{$fileName} /tmp", $output, $statusCode);
        $contents = preg_grep('/ERROR/', file("/tmp/{$fileName}"));
        foreach ($contents as $line) {
            $error = preg_replace("/\[.+?\] ingest.ERROR: /", "", trim($line));
            echo "\t$error\n";
            if (preg_match("/VOD metadata for programme ([^ ]+)/", $error, $matches)) {
                $missingMeta[] = $matches[1];
            }
        }
    }
}

function rclone(string $cmd, ?array &$output, ?int &$responseCode)
{
    $output = [];
    exec("rclone {$cmd} 2>&1", $output, $responseCode);
}