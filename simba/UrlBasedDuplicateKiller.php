<?php
require_once('vendor/autoload.php');
use Doctrine\DBAL\Connection;

$db = \Doctrine\DBAL\DriverManager::getConnection([
    'driver' => 'pdo_mysql',
    'user' => 'root',
    'password' => '7efG7EEa&rFyKtR',
    'port' => 7001,
    'host' => '127.0.0.1',
    'dbname' => 'switch_content',
]);

$ftp = ftp_connect("antennaavod.ftp.upload.akamai.com");
ftp_login($ftp, "switchtv", "@V0d2021!");
$akamaiList = ftp_nlist($ftp, "/1167027/356");

$HLSPlaybackKeyId = 4777;
$DashPlaybackKeyId = 4778;
$encodingAssetIdKeyId = 4782;

$result = $db->query("
    select v.videoID,v.state,
        (select value from switch_key_values.string where videoID=v.videoID and keyId={$HLSPlaybackKeyId}) as HLS,
        (select value from switch_key_values.string where videoID=v.videoID and keyId={$HLSPlaybackKeyId}) as dash,
        (select value from switch_key_values.integer where videoID=v.videoID and keyId={$encodingAssetIdKeyId}) as encodingAssetId
    from videos v 
    where v.siteID in (353) 
        and v.state in ('APPROVED', 'PENDING')
        and v.external_ref in ('TV', 'MOVIE')
");

$missingFromAkamai = [];
$inAkamaiUnreferenced = [];
$referencedEncodingAssets = [];
$mismatchCount = 0;
file_put_contents('encodingAssetIdFixup.sql', '');
foreach ($result->fetchAll() as $row) {
    $vodAssetId = $row['videoID'];
    $hlsUrl = trim($row['HLS']);
    $dashUrl = trim($row['dash']);
    $encodingAssetId = $row['encodingAssetId'];

    if (!$hlsUrl) {
        echo "[$vodAssetId] NO HLS URL\n";
    }
    if (!$dashUrl) {
        echo "[$vodAssetId] NO DASH URL\n";
    }
    if (!$encodingAssetId) {
        echo "[$vodAssetId] NO ENCODING ID\n";
    }

    $hlsAssetId = @explode('/', $hlsUrl)[2];
    $dashAssetId = @explode('/', $dashUrl)[2];

    if (!$hlsAssetId && $hlsUrl) {
        echo "[$vodAssetId] BAD HLS URL: Can't get id from $hlsUrl\n";
    }
    if (!$dashAssetId && $dashUrl) {
        echo "[$vodAssetId] BAD DASH URL: Can't get id from $dashUrl\n";
    }

    if ($hlsAssetId != $dashAssetId) {
        $mismatchCount++;
        echo "[$vodAssetId] URL MISMATCH: HLS=$hlsUrl, Dash=$dashUrl\n";
    }
    if ($encodingAssetId != $hlsAssetId || $encodingAssetId != $dashAssetId) {
        $mismatchCount++;
        echo "[$vodAssetId] MISMATCH: Playback Id=$hlsAssetId, EncodingId=$encodingAssetId\n";

        file_put_contents(
            'encodingAssetIdFixup.sql',
            "insert into switch_key_values.integer (keyID, videoID, value) values ($encodingAssetIdKeyId, $vodAssetId, $hlsAssetId);\n",
            FILE_APPEND
        );
    }

    $referencedEncodingAssets = array_unique(array_merge(
        $referencedEncodingAssets,
        array_unique([ $encodingAssetId, $hlsAssetId, $dashAssetId ])
    ));

    foreach ([$encodingAssetId, $hlsAssetId, $dashAssetId ] as $id) {
        if ($id && !in_array($id, $akamaiList)) {
            $missingFromAkamai[$id] = $id;
        }
    }

    /*
    $encodingAssetBackReference = $db->query("
        select value from switch_key_values.string where videoID={$encodingAssetId} and keyId = 4807
        union
        select value from switch_key_values.integer where videoID={$encodingAssetId} and keyId = 4765
    ")->fetchColumn();

    if ($encodingAssetBackReference != $vodAssetId) {
        echo "Encoding asset backreference is incorrect. {$encodingAssetId} Points to {$encodingAssetBackReference} instead of $vodAssetId\n";
        $badBackRefCount++;
    }
    $encodingAssetState = (string) $db->query("select state from videos where videoID={$encodingAssetId}")->fetchColumn();
    echo "$encodingAssetId - $encodingAssetState\n";
    */

}

if ($mismatchCount) {
    echo "Mismatch count:  $mismatchCount\n";
}

$inAkamaiUnReferenced = array_values(array_diff($akamaiList, $referencedEncodingAssets));

//echo "Assets not in akamai\n";
//print_r(array_values($missingFromAkamai));

echo "Assets to delete from akamai\n";
sort($inAkamaiUnReferenced);
//print_r($inAkamaiUnReferenced);


foreach ($inAkamaiUnReferenced as $id) {
    if (empty($id)) {
        continue;
    }

    //echo "curl http://vod-origin/delete/356/$id\n";
}
