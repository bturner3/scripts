<?php
date_default_timezone_set('Australia/Sydney');
$pendingFiles = [];
//$pendingDirs = [ 'vod/cordoned', 'vod/To_upload_210601', 'vod/uploading', 'vod/uploading/mprousko_S4_SD', 'vod/update-mxf-only', 'vod/pending', 'vod2/pending', 'vod3/pending', 'vod4/pending', 'vod5/pending', 'vod-priority/pending' ];
$pendingDirs = [ 'vod/pending', 'vod2/pending', 'vod3/pending', 'vod4/pending', 'vod5/pending', 'vod-priority/pending' ];
$failedDirs = [ 'vod/failed', 'vod2/failed', 'vod3/failed', 'vod4/failed', 'vod5/failed', 'vod-priority/failed' ];

$len = 0;
foreach ($pendingDirs as $dir) {
    if (strlen($dir) > $len) {
        $len = strlen($dir);
    }
}

$pendingFiles = collectFiles($pendingDirs, '/\.(mxf|processed)$/');
$failedFiles = collectFiles($failedDirs, '/\.mxf$/');

// Get failed file stats
$failureSize = 0;
$failureCount = count($failedFiles);
foreach ($failedFiles as $failedFile) {
    $failureSize += $failedFile['Size'];
}

// Arrange by name for ease of removing processed files
$pendingByName = [];
foreach ($pendingFiles as $file) {
    $pendingByName[$file['Name']] = $file;
}

// Find and filter processed files
$processedFileCount = 0;
foreach ($pendingByName as $name => $file) {
    if (preg_match("/\.processed$/", $name)) {
        unset($pendingByName[$name]);

        $processedFileName = str_replace('.processed', '', $name);
        unset($pendingByName[$processedFileName]);
        $processedFileCount++;
    }
}

$fileCount = count($pendingByName);
$totalSize = 0;
$detailsByFolder = [];
foreach ($pendingByName as $file) {
    $totalSize += $file['Size'];
    if (empty($detailsByFolder[$file['dir']])) {
        $detailsByFolder[$file['dir']] = [ 'files' => 0, 'size' => 0 ];
    }
    $detailsByFolder[$file['dir']]['files']++;
    $detailsByFolder[$file['dir']]['size'] += $file['Size'];
}

exec('clear');
echo "Time: " . date("Y-m-d H:i:s") . "\n";
echo sprintf("+" . str_repeat('-', $len + 2) . "+-----+----------+\n");
echo sprintf("| %{$len}s | %3s | %8s |\n", 'Dir', '#', 'Size');
echo sprintf("+" . str_repeat('-', $len + 2) . "+-----+----------+\n");
foreach ($detailsByFolder as $dir => $details) {
    echo sprintf("| %{$len}s | %3s | %8s |\n", $dir, $details['files'], formatBytes($details['size']));
}
echo sprintf("+" . str_repeat('-', $len + 2) . "+-----+----------+\n");
echo sprintf("| %{$len}s | %3d | %8s |\n", "Sub Total", $fileCount, formatBytes($totalSize));
echo sprintf("| %{$len}s | %3d | %8s |\n", "Failed", $failureCount, formatBytes($failureSize));
echo sprintf("+" . str_repeat('=', $len + 2) . "+=====+==========+\n");
echo sprintf("| %{$len}s | %3d | %8s |\n", "Total", $fileCount + $failureCount, formatBytes($totalSize + $failureSize));

echo "\nUnmoved processed files: " . $processedFileCount . "\n";

function rclone(string $cmd, ?array &$output, ?int &$responseCode)
{
    $output = [];
    #echo "rclone {$cmd} 2>&1\n";
    exec(
        "rclone {$cmd} 2>&1",
        $output,
        $responseCode
    );
}

function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

function collectFiles(array $dirs, $filter)
{
    $allFiles = [];
    foreach ($dirs as $dir) {
        //echo "["  . date('Y-m-d H:i:s') . "] Listing files in $dir\n";
        rclone("lsjson s3-antenna-ingest:antennatv-sftp/content/{$dir}", $output, $responseCode);

        $files = json_decode(implode('', $output), true);
        if (!is_array($files)) {
            echo "Something went wrong processing $dir\n";
            var_dump($output);
            exit;
        }

        foreach ($files as &$file) {
            $file['dir'] = $dir;
        }
        $allFiles = array_merge($allFiles, $files);
    }

    return array_filter($allFiles, function ($file) use ($filter) {
        return preg_match($filter, $file['Name']);
    });
}