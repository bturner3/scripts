<?php
$sqlLog = fopen('sqlLog', 'a');
$dbPass = getenv('TONTON_DB_PASS');
$adTags = [
    // sprintf($adTag, urlencode(genre=$genre))
    'PREROLL' => "https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/Tonton_AVOD_Video&description_url=https%%3A%%2F%%2Fwww.tonton.com.my%%2F&tfcd=0&npa=0&sz=770x435&cust_params=%s&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=1&env=vp&impl=s&correlator=&ad_rule=0",

    // sprintf($adTag, urlEncode(genre=$genre), $midrollNumber)
    'MIDROLL' => "https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/Tonton_AVOD_Video&description_url=https%%3A%%2F%%2Fwww.tonton.com.my%%2F&tfcd=0&npa=0&sz=770x435&cust_params=%s&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=midroll&pod=%d&ppos=1&env=vp&impl=s&correlator=&ad_rule=0",

    // sprintf($adTag, urlencode(genre=$genre))
    'POSTROLL' => 'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/Tonton_AVOD_Video&description_url=https%%3A%%2F%%2Fwww.tonton.com.my%%2F&tfcd=0&npa=0&sz=770x435&cust_params=%s&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=postroll&env=vp&impl=s&correlator=&ad_rule=0'
];

$genreMap = [
    'AVOD_DRAMA' => 'Drama',
    'AVOD_MOVIE' => 'Movie',
    'TV3' => 'Entertainment',
    'TV9' => 'Entertainment',
    '8TV' => 'Entertainment',
    'TONTON_SEL' => 'Movie',
    'TONTON' => 'Entertainment',
    'TONTON_ORI_FREE' => 'Drama',
    'AVOD_ENTERTAINMENT' => 'Entertainment'
];
if (!$dbPass) {
    die("TONTON_DB_PASS env var must be set\n");
}

$db = new PDO(
    'mysql:dbname=switch_content;host=127.0.0.1;port=7008',
    'root',
    $dbPass
);

$videosWithAds = $db->query(
    "select s.keyId, v.videoId, v.source_url as channel, s.value as ADVERTISING_BLOB
    from switch_key_values.string s
    join switch_content.videos v on s.videoId=v.videoId 
    where keyId in (5147, 5196);"
)->fetchAll(PDO::FETCH_ASSOC);

$total = count($videosWithAds);
echo "Processing $total videos with current ad blobs\n";
$count = 0;
foreach ($videosWithAds as $video) {
    $count++;
    $adBlob = json_decode($video['ADVERTISING_BLOB'], true);
    if (empty($adBlob)) {
        echo sprintf("[%04d/%d] Skipping empty ad blob\n", $count, $total);
        continue;
    }

    $channel = $video['channel'];
    $genre = $genreMap[$channel];
    $preRoll = $postRoll = true; // Ad pre and post roll for all ads
    $midRolls = [];
    foreach ($adBlob as $ad) {
        switch ($ad['position']) {
            case 'PREROLL':
                $preRoll = true;
                break;
            case 'POSTROLL':
                $postRoll = true;
                break;
            default:
                $midRolls[] = $ad['position'];
                break;
        }
    }

    $newAdBlob = [];
    $params =  urlencode('genre=' . $genre);
    if ($preRoll) {
        $newAdBlob[] = [
            'position' => 'PREROLL',
            'url' => sprintf($adTags['PREROLL'], $params)
        ];
    }

    if ($midRolls) {
        $midRollPosition = 1;
        foreach ($midRolls as $midRoll) {
            $newAdBlob[] = [
                'position' => $midRoll,
                'url' => sprintf($adTags['MIDROLL'], $params, $midRollPosition)
            ];
            $midRollPosition++;
        }
    }

    if ($postRoll) {
        $newAdBlob[] = [
            'position' => 'POSTROLL',
            'url' => sprintf($adTags['POSTROLL'], $params)
        ];
    }

    $newAdBlob = json_encode($newAdBlob);
    if ($newAdBlob !== $video['ADVERTISING_BLOB']) {
        $sql = "update switch_key_values.string set value='$newAdBlob' where videoId={$video['videoId']} and keyId={$video['keyId']};";
        $db->query($sql);
        //fwrite($sqlLog, $sql . "\n");
        echo sprintf("[%04d/%d] Updated ad blob for video '%s'\n", $count, $total, $video['videoId']);
    }
}
