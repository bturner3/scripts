<?php
// Super simplistic EPG populator
// Hack in the desired api host, channels, style, set some content that will be randomly scattered through the guide
// set SIMBA_API_KEY in env and run it
const STYLE_MINIMAL = 'MINIMAL';
const STYLE_FULL = 'FULL';

$start = time() - 12 * 3600;
$end = time() + 3 *  24 * 3600;
//$apiHost = 'http://localhost:8013';
$apiHost = 'http://ingest-api-brad.mediahq-demo.switch.tv';
$channels = [ 'ABC' ];
$durations = [ 20, 30, 60, 120 ];
$auth = getenv('SIMBA_API_KEY');

if (!$auth) { 
    echo "Set desired api key in SIMBA_API_KEY env var";
    exit;
}

$style = STYLE_FULL;

if ($style == 'MINIMAL') {
    $titles = [
        'Mystery Road',
        'The Heart Guy',
        'ABC News',
        'Landline',
        'Insiders',
        'Offsiders',
        'News Breakfast Early',
        'The World',
        'The Drum'
    ];
} else {
    $titles = [
        'NEWS' => ['Rating' => 'PG', 'Advisory' => 'The world is scary', 'Genre' => 'News', 'Synopsis' => 'News... about things'],
        'Supernatural' => ['Rating' => 'MA15+', 'Advisory' => 'Contains Violence, Sexual themes', 'Genre' => 'Drama, Fantasy, Horror', 'Synopsis' => 'Two brothers follow their fathers footsteps as hunters'],
        'Doctor Who' => ['Rating' => 'PG', 'Advisory' => 'Contains all kinds of crazy', 'Genre' => 'Adventure, Drama, Family', 'Synopsis' => 'The further adventures in time and space o the doctor'],
        'Elementary' => ['Rating' => 'MA15+', 'Advisory' => '', 'Genre' => 'Crime, Drama, Mystery', 'Synopsis' => 'A modern take on the cases of Sherlock Holmes'],
        'Whose line is it anyway?' => ['Rating' => 'G', 'Advisory' => '', 'Genre' => 'Comedy, Game-Show', 'Synopsis' => 'An improv skit comedy show'],
        'Impress Me' => ['Rating' => 'G', 'Advisory' => '', 'Genre' => 'Comedy', 'Synopsis' => 'Two impressionists start a 12-step program to stop doing impressions and transition to dramatic acting']
    ];
}

foreach ($channels as $channel) {
    $s = $start;

    while ($s < $end) {
        $eventID = $channel . rand(1, 99999);
        $duration = array_rand(array_flip($durations)) * 60;
        $e = $s + $duration;
        if ($style === STYLE_MINIMAL) {

            $title = $titles[array_rand($titles)];
            $body = "<EPGEvent>
                <channelTag>{$channel}</channelTag>
                <title>$title</title>
                <start>" . date('c', $s) . "</start>
                <end>" . date('c', $e) . "</end>
            </EPGEvent>";
        } else {
            $title = array_keys($titles)[array_rand(array_keys($titles))];
            $seasonNum = rand(1, 10);
            $episodeNum = rand(1, 22);
            $body = "<EPGEvent>
                <eventID>{$eventID}</eventID>
                <channelTag>{$channel}</channelTag>
                <title>$title</title>
                <genre>{$titles[$title]['Genre']}</genre>
                <synopsis>{$titles[$title]['Synopsis']}</synopsis>
                <parentalRating>{$titles[$title]['Rating']}</parentalRating>
                <parentalAdvice>{$titles[$title]['Advisory']}</parentalAdvice>
                <closedCaption>" . rand(0, 1) . "</closedCaption> 
                <highDefinition>" . rand(0, 1) . "</highDefinition> 
                <start>" . date('c', $s) . "</start>
                <end>" . date('c', $e) . "</end>
                <season><number>$seasonNum</number></season>
                <episode>
                  <title>$title: Season $seasonNum, Episode $episodeNum</title>
                  <number>$episodeNum</number>
                </episode>
                </EPGEvent>";
        }


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$apiHost/index.php/epg",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/xml",
                "x-switch-api-key: $auth"
            ),
        ));

        $response = curl_exec($curl);
        $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        echo "[$responseCode][$channel:$eventID] $s - $e ($duration)\n";

        if ($responseCode != 201) {
            echo $body . "\n\n";
            echo $response;
        }
        $s = $e;
    }
}
