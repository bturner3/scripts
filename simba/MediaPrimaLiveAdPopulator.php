<?php
$dbPass = getenv('TONTON_DB_PASS');

if (!$dbPass) {
    die("TONTON_DB_PASS env var must be set\n");
}

$db = new PDO(
    'mysql:dbname=switch_content;host=127.0.0.1;port=7008',
    'root',
    $dbPass
);



$adTags = [
    'TV3' => [
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/TV3_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=1&env=vp&impl=s&correlator=&ad_rule=0',
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/TV3_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=2&env=vp&impl=s&correlator=&ad_rule=0'
    ],
    'NTV7' => [
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/NTV7_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=1&env=vp&impl=s&correlator=&ad_rule=0',
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/NTV7_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=2&env=vp&impl=s&correlator=&ad_rule=0'
    ],
    '8TV' => [
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/8TV_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=1&env=vp&impl=s&correlator=&ad_rule=0',
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/8TV_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=2&env=vp&impl=s&correlator=&ad_rule=0'
    ],
    'TV9' => [
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/TV9_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=1&env=vp&impl=s&correlator=&ad_rule=0',
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/TV9_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=2&env=vp&impl=s&correlator=&ad_rule=0'
    ],
    'DS' => [
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/Drama_Sangat_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=1&env=vp&impl=s&correlator=&ad_rule=0',
        'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/Drama_Sangat_LiveTV&description_url=https%3A%2F%2Fwww.tonton.com.my%2F&tfcd=0&npa=0&sz=770x435&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=2&env=vp&impl=s&correlator=&ad_rule=0'
    ]
];

$keyId = 5219;
foreach ($adTags as $channelId => $preRolls) {
    $videoIds = $db->query("select videoId from switch_content.videos where siteId=378 and external_id='$channelId'")->fetchAll(PDO::FETCH_ASSOC);
    $adBlob = json_encode([
        [ 'position' => 'PREROLL', 'url' => $preRolls[0] ],
    ]);

    foreach ($videoIds as $videoId) {
        $videoId = $videoId['videoId'];
        $existing = $db->query("select * from switch_key_values.string where videoID=$videoId and keyID=$keyId")->fetchAll(PDO::FETCH_ASSOC);

        if ($existing) {
            $sql = "Update switch_key_values.string set value='$adBlob' where keyId=$keyId and videoId=$videoId limit 1;";
        } else {
            $sql = "insert into switch_key_values.string (keyId, videoId, value) values ($keyId, $videoId, '$adBlob');";
        }
        echo $sql . "\n";
    }
}
