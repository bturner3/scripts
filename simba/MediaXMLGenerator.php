<?php

mkdir('./mediaXMLs');
foreach (file('files') as $file) {
    $file = "media/" . trim($file);
    $mediaId = substr(basename($file),0 ,-4);

    $xml = "<xml>
  <dataSourceName>mhq-demo-ingest-old</dataSourceName>
  <location>$file</location>
  <mediaId>$mediaId</mediaId>
</xml>
";

    file_put_contents("./mediaXMLs/$mediaId.media.xml", $xml);
}