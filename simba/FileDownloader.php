<?php
$sourceApiBase = 'ingest-api.mediahq-demo.switch.tv/index.php';
$targetApiBase = 'ingest-api.mediahq-demo-lke.switch.tv/index.php';

$sites = [
    354 => 343,
    355 => 344,
    357 => 346
];
foreach ($sites as $sourceSiteId => $targetSiteId) {
    $files = json_decode(get($sourceApiBase . "/site/$sourceSiteId/fileMapping/list?videoId=0"));

    foreach ($files as $file) {

        $mapId = $file->id;
        $downloadUrl = "https://vcms.mediahq-demo.switch.tv/filestore.php?siteID=$sourceSiteId&fileStoreMapID=$mapId";
        $target = "/tmp/siteFiles/$sourceSiteId/" . $file->file->originalName;

        try {
            get($downloadUrl, false, $target);
            echo "successfully downloaded $target from old fil store\n";
            $res = json_decode(upload($target));
            $uploadedFileId = $res->file->id;
            echo "successfully uploaded $target to new file store\n";
            $res = createFileMapping($targetSiteId, $uploadedFileId, 0, $file->group_tag, $file->tag);
            $map = json_decode($res)->fileMapping;
            echo "Created file mapping {$map->groupTag}:{$map->tag} on site {$map->siteId} to file {$map->fileId}\n";
        } catch (Exception $e) {
            echo "Failed on $target\n";
            unlink($target);
        }
    }
}

function get($url, $auth = true, $downloadTarget = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    if ($downloadTarget) {
        curl_setopt($ch, CURLOPT_FILE, fopen($downloadTarget, 'w'));
    } else {
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    }

    if ($auth) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, [ 'x-switch-api-key: demo' ]);
    }
    $res = curl_exec($ch);

    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($status === 204) {
        return '[]';
    } elseif ($status !== 200) {
        throw new Exception($res);
    }

    return $res;
}


function upload($filename) {
    global $targetApiBase;

    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => "$targetApiBase/file",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => [ 'file'=> new CURLFILE($filename) ],
        CURLOPT_HTTPHEADER => [ 'x-switch-api-key: demo' ],
    ));

    $response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($status > 300) {
        throw new Exception($response);
    }

    return $response;
}

function createFileMapping($siteId, $fileId, $videoId, $groupTag, $tag)
{
    global $targetApiBase;

    $ch = curl_init();
    $body = [
        'siteId' => $siteId,
        'fileId' => $fileId,
        'videoId' => $videoId,
        'groupTag' => $groupTag,
        'tag' => $tag
    ];
    curl_setopt_array($ch, array(
        CURLOPT_URL => "$targetApiBase/fileMapping",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($body),
        CURLOPT_HTTPHEADER => [
            'x-switch-api-key: demo',
            'Content-Type: application/json'
        ],
    ));

    $response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($status > 300) {
        throw new Exception($response);
    }

    return $response;
}