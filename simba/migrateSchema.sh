export SOURCE_DB_USER=linroot
export SOURCE_DB_PASS=rbJG2hXAWsSYR^re
export SOURCE_DB_PORT=3306
export SOURCE_DB_HOST=lin-5642-4228-mysql-primary.servers.linodedb.net

export TARGET_DB_USER=linroot
export TARGET_DB_PASS=L8Pm^hsUbDB12bEY
export TARGET_DB_PORT=3306
export TARGET_DB_HOST=lin-7326-4945-mysql-primary.servers.linodedb.net

# export from source
echo "Running mysqldump\n";
mysqldump \
    -u$SOURCE_DB_USER -p"$SOURCE_DB_PASS" -h$SOURCE_DB_HOST -P$SOURCE_DB_PORT \
    --databases `mysql -u$SOURCE_DB_USER -p"$SOURCE_DB_PASS" -h$SOURCE_DB_HOST -P$SOURCE_DB_PORT --skip-column-names -e "
        SELECT GROUP_CONCAT(schema_name SEPARATOR ' ') FROM information_schema.schemata WHERE schema_name NOT IN ('mysql','performance_schema','information_schema', 'sys');
    "` \
    --skip-column-statistics \
    --set-gtid-purged=OFF \
    --no-data > schema.sql

echo "Dump complete - running import\n";
# import to target
mysql -u$TARGET_DB_USER -p"$TARGET_DB_PASS" -h$TARGET_DB_HOST -P$TARGET_DB_PORT < schema.sql
echo "Done\n";

