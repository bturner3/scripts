<?php
if (empty($argv[1])) {
    die("Input file not specified\n");
}
$currentVersionId = '';
$host = "http://ingest-api.mediaprima-prod.switch.tv";
$adTags = [
    // sprintf($adTag, urlencode(genre=$genre))
    'PREROLL' => "https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/Tonton_AVOD_Video&description_url=https%%3A%%2F%%2Fwww.tonton.com.my%%2F&tfcd=0&npa=0&sz=770x435&cust_params=%s&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=preroll&ppos=1&env=vp&impl=s&correlator=&ad_rule=0",

    // sprintf($adTag, urlEncode(genre=$genre), $midrollNumber)
    'MIDROLL' => "https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/Tonton_AVOD_Video&description_url=https%%3A%%2F%%2Fwww.tonton.com.my%%2F&tfcd=0&npa=0&sz=770x435&cust_params=%s&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=midroll&pod=%d&ppos=1&env=vp&impl=s&correlator=&ad_rule=0",

    // sprintf($adTag, urlencode(genre=$genre))
    'POSTROLL' => 'https://pubads.g.doubleclick.net/gampad/ads?iu=/1009103/Tonton_AVOD_Video&description_url=https%%3A%%2F%%2Fwww.tonton.com.my%%2F&tfcd=0&npa=0&sz=770x435&cust_params=%s&gdfp_req=1&output=vast&unviewed_position_start=1&vpos=postroll&env=vp&impl=s&correlator=&ad_rule=0'
];
$genreMap = [
    'AVOD_DRAMA' => 'Drama',
    'AVOD_MOVIE' => 'Movie',
    'TV3' => 'Entertainment',
    'TV9' => 'Entertainment',
    '8TV' => 'Entertainment',
    'TONTON_SEL' => 'Movie',
    'TONTON' => 'Entertainment',
    'TONTON_ORI_FREE' => 'Drama',
    'AVOD_ENTERTAINMENT' => 'Entertainment',
    'TONTON_EXC_FREE' => 'Drama',
    'SVOD_MOVIE' => 'NONE',
];
$apiKey = 'w9VrFggU';

$file = $argv[1];

$fh = fopen($file, 'r');
$fields = fgetcsv($fh);

while ($line = fgetcsv($fh)) {
    //echo "-------------------------------\n";
    $line = array_combine($fields, $line);
    if (empty($line['type'])) {
        echo "Empty type encountered - assuming end of file\n";
        break;
    }

    try {
        $body = parse($line);
        send($body);
    } catch (Exception $e) {
        echo "[FAILED] Unable to create $currentVersionId\n\t " . $e->getMessage() . "\n";
    }
}

function generateProgrammeId($line) {
    $showId = trim($line['showId']);
    return match ($line['type']) {
        'MOVIE' => $showId . '-movie',
        'SERIES' =>  $showId . '-series',
        'SEASON' => $showId . '-S' . $line['season'],
        'TV' => $showId . '-S' . $line['season'] . 'E' . $line['episode']
    };
}

function parse($data) {
    global $currentVersionId;
    $keyMap = [
        'ShowTitle' => 'showTitle',
        'Year' => 'year',
        'Language' => 'language',
    ];
    $removeKeys = [ '', 'Media File Name', 'Portrait Image Name', 'Landscape Image' ];

    if (empty($data['programmeId'])) {
        $data['programmeId'] = generateProgrammeId($data);
    }

    if (empty($data['versionId'])) {
        $data['versionId'] = $data['programmeId'] . '-v1';
    }

    $currentVersionId = $data['versionId'];
    $data['parentalRating'] = match ($data['parentalRating']) {
        '13+' => 'P13',
        default => $data['parentalRating']
    };

    $cast = extractCast($data);
    $adBlob = extractAdBlob($data);
    $exhibitionWindow = extractExhibitionWindow($data);
    $aspect = extractAspectRatio($data);
    foreach ($keyMap as $oldKey => $newKey) {
        $data[$newKey] = $data[$oldKey];
        unset($data[$oldKey]);
    }
    foreach ($removeKeys as $key) {
        unset($data[$key]);
    }

    if ($data['type'] === 'SERIES' || $data['type'] === 'SEASON') {
        $data['title'] = $data['showTitle'];
        unset($data['showTitle']);
    }


    $body = [
        'exhibitionWindow' => $exhibitionWindow,
        'cast' => $cast,
        'advertising' => $adBlob,
        'aspectRatio' => $aspect,
        ...array_map('trim', $data)
    ];

    return $body;
}

function extractExhibitionWindow(&$data) {
    date_default_timezone_set("Asia/Kuala_Lumpur");
    $start = date('c', strtotime(str_replace('/', '-', $data['exhibitionWindow Start'])));
    $end = date('c', strtotime(str_replace('/', '-', $data['exhibitionWindow End'])));

    $window = [ 'start' => $start, 'end' => $end ];
    unset($data['exhibitionWindow Start'], $data['exhibitionWindow End']);
    return $window;
}

function extractCast(&$data) {
    $cast = [];
    foreach ($data as $key => $value) {
        if (stristr($key, 'cast/') && $value) {
            $cast[substr($key, 5)] = $value;
        }
    }
    $data = array_filter($data, function($key) { return !stristr($key, 'cast/'); }, ARRAY_FILTER_USE_KEY);

    return $cast;
}

function extractAdBlob(&$data) {
    global $genreMap, $adTags;

    $channel = $data['channel'];
    $adBlob = [];

    $genre = $genreMap[$channel] ?? null;
    if ($genre === 'NONE') {
        unset($data['advertisingMidpoints']);
        return [];
    } else if (!$genre) {
        throw new Exception("No genre specified for channel $channel. Unable to generate ad tags");
    }
    $midRollPod = 1;
    $midpoints = $data['advertisingMidpoints']
               ? array_map('trim', explode(',', $data['advertisingMidpoints']))
               : [];
    $adMarkers = [ 'PREROLL', ...$midpoints, 'POSTROLL' ];
    foreach ($adMarkers as $marker) {
        $marker = trim($marker);
        if (is_numeric($marker)) {
            $marker *= 1000;
        }

        if (!empty($marker)) {
            $type = is_numeric($marker) ? 'MIDROLL' : strtoupper($marker);
            if (empty($adTags[$type])) {
                throw new Exception("No ad URLS defined for $channel:$type");
            }

            if ($type === 'MIDROLL') {
                $adUrl = sprintf($adTags[$type], urlencode('genre=' . $genre), $midRollPod);
                $midRollPod++;
            } else {
                $adUrl = sprintf($adTags[$type], urlencode('genre=' . $genre));
            }
            $adBlob[] = [
                'position' => (string) $marker,
                'url' => $adUrl
            ];
        }
    }
    unset($data['advertisingMidpoints']);
    return $adBlob;
}
function extractAspectRatio(&$data) {
    $aspect = null;
    if ($data['aspectRatio']) {
        list ($width, $height) = explode(':', $data['aspectRatio']);
        $aspect = [
            'width' => $width,
            'height' => $height
        ];
    }
    unset($data['aspectRatio']);

    return $aspect;
}
function send($requestBody, $attempt = 1) {
    global $host, $apiKey;

    $url = trim($host, '/') . "/index.php/vodAsset?format=json";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestBody));
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type:application/json',
        "x-switch-api-key:$apiKey"
    ]);
    //echo json_encode($requestBody) . "\n";
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    $versionId = $requestBody['versionId'] ?? '';
    if ($status === 200) {
        echo "[SUCCESS] Updated $versionId\n";
    } elseif ($status === 201) {
        echo "[SUCCESS] Created $versionId\n";
    } else {
        $errors = json_decode($result)->errors ?? null;
        if (!$errors && $attempt <= 3) {
            echo "[ERROR] Probably transient error creating $versionId - Retrying\n";
            send($requestBody, $attempt + 1);
        } else {
            echo "[FAILED] Unable to create $versionId\n";
            if ($errors) {
                $errors = array_map(function($error) {
                    unset($error->trace);
                    return $error;
                }, $errors);
                echo "\t" . json_encode($errors) . "\n";
            }
        }
    }
}
