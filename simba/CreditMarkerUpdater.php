<?php
$sqlLog = fopen('sqlLog', 'a');
$file = fopen('markers.csv', 'r');

$dbPort = getenv('DB_PORT');
$dbPass = getenv('DB_PASS');

if (!$dbPass || !$dbPort) {
    die("DB_PORT and DB_PASS env vars must be set\n");
}
$db = new PDO(
    'mysql:dbname=switch_content;host=127.0.0.1;port=' . $dbPort,
    'root',
    $dbPass
);

$skipUntil = "";
$skip = !empty($skipUntil);
$errors = [];
while (list ($programmeId, $introEnd, $creditsStart) = fgetcsv($file)) {
    if ($skip) {
        if (strtolower($programmeId) === strtolower($skipUntil)) {
            $skip = false;
            echo "[INFO] Found $programmeId -> resuming\n";
        }
        continue;
    }

    if (!is_numeric($introEnd) || !is_numeric($creditsStart)) {
        echo "ERROR: [$programmeId] Invalid markers. introEnd={$introEnd}, creditsStart={$creditsStart}\n";
        continue;
    }

    $introEnd = (int) $introEnd;
    $creditsStart = (int) $creditsStart;

    $sql = "select siteId,videoId,duration from switch_content.videos where extra_field3='$programmeId' and siteId in (353, 354)";
    ///$videoIds = $db->executeQuery($sql)->fetchAllKeyValue();
    $results = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    if (empty($results)) {
        echo "ERROR: [$programmeId] no vod assets found\n";
        continue;
    }

    $duration = null;
    foreach ($results as $row) {
        $videoIds[$row['siteId']] = $row['videoId'];
        if (!$duration && $row['duration']) {
            $duration = $row['duration'] * 1000;
        }
    }

    if ($duration && $creditsStart > $duration) {
        echo "ERROR: [$programmeId] Credit marker ($creditsStart) greater than duration ($duration)\n";
        continue;
    } elseif ($creditsStart < ($duration - 300000)) {
        echo "WARNING: [$programmeId] Credit marker ($creditsStart) is more than 5 minutes less than duration ($duration)\n";
    }

    $markers = [];
    if ($introEnd > 0) {
        $markers[] = [ 'name' => 'introEnd', 'position' => $introEnd ];
    }
    if ($creditsStart > 0) {
        $markers[] = [ 'name' => 'creditsStart', 'position' => $creditsStart ];
    }
    $bookmarkBlob = json_encode($markers);

    $sql = "insert into switch_key_values.string (videoId, keyId, value) VALUES ($videoIds[353], 4859, '$bookmarkBlob')";
    if (isset($videoIds[354])) {
        $sql .= ",  ($videoIds[354], 4860, '$bookmarkBlob')";
    }
    $sql .= " ON DUPLICATE KEY update value='$bookmarkBlob'";
    //echo $sql . "\n";
    fwrite($sqlLog, "--$programmeId\n");
    fwrite($sqlLog, $sql . "\n");
    $db->query($sql);
    echo "SUCCESS: [$programmeId] updated\n";
}