<?php
require_once 'vendor/autoload.php';
date_default_timezone_set('UTC');

$db = \Doctrine\DBAL\DriverManager::getConnection([
    'driver' => 'pdo_mysql',
    'user' => 'root',
    'host' => '127.0.0.1',
    'dbname' => 'switch_content',

    # Antenna Prod
    'password' => 'JMfJRJxGaxfajg3b',
    'port' => 7002,
]);

$sql = "select * from switch_content.schedule where state='PENDING'";
$rawEvents = $db->executeQuery($sql)->fetchAllAssociative();
$events = [];
foreach ($rawEvents as $event) {
    $events[$event['videoID']][$event['action']] = $event;
}


$sql = "select siteID,videoID, external_ref as type, state,
	(select value from switch_key_values.string kv where kv.videoId=v.videoId and kv.keyID in (4770, 4784)) as EXHIBIT_START,
 	(select value from switch_key_values.string kv where kv.videoId=v.videoId and kv.keyID in (4771, 4785)) as EXHIBIT_END
from switch_content.videos v
where siteID = 353 and external_ref in ('TV', 'MOVIE') and state not in ('DELETED', 'EXPIRED'); ";
$rows = $db->executeQuery($sql)->fetchAllAssociative();


file_put_contents('events.sql', '');
foreach ($rows as $count => $row) {
    $videoId = $row['videoID'];
    $publishEvent = $events[$videoId]['PUBLISH'] ?? null;
    $expireEvent = $events[$videoId]['EXPIRE'] ?? null;

    $start = strtotime($row['EXHIBIT_START']);
    $end = strtotime($row['EXHIBIT_END']);
    $now = time();
    if ($start > $now) {
        if (!$publishEvent) {
            echo "ERROR: MISSING PUBLISH EVENT ON $videoId\n";
        } elseif ($start !== strtotime($publishEvent['eventDateTime'])) {
            echo "ERROR: INCORRECT PUBLISH EVENT TIME ON $videoId -- {$row['EXHIBIT_START']} vs {$publishEvent['eventDateTime']}\n";
        }
    }

    if ($end > $now) {
        if (!$expireEvent) {
            echo "ERROR: MISSING EXPIRE EVENT ON $videoId\n";
        } elseif ($end !== strtotime($expireEvent['eventDateTime'])) {
            $correctTime = date("Y-m-d H:i:s", $end);

            echo "ERROR: INCORRECT EXPIRE EVENT TIME ON $videoId -- {$row['EXHIBIT_END']} vs {$expireEvent['eventDateTime']}. Setting to $correctTime\n";
            $fixSql = "update switch_content.schedule set eventDateTime='$correctTime' where id={$expireEvent['id']};\n";

            //file_put_contents('events.sql', $fixSql, FILE_APPEND);
            $db->executeQuery($fixSql);
        }
    }
}