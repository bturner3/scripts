<?php
$file = fopen('markers.csv', 'r');
$seen = [];

while (@list($programmeId, $introEnd, $creditsStart) = fgetcsv($file)) {
    if ($creditsStart < 500000) {
        echo "$programmeId, $introEnd, $creditsStart\n";
    } 
    continue;
    if (isset($seen[$programmeId])) {
        $old = $seen[$programmeId];
        $new = "$programmeId, $introEnd, $creditsStart";
        if ($old != $new) {
            echo "DUPLICATE:\n\t$seen[$programmeId]\n\t$programmeId, $introEnd, $creditsStart\n";
        }
    }
    $seen[$programmeId] = "$programmeId, $introEnd, $creditsStart";
}
