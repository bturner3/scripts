<?php
date_default_timezone_set('Australia/Sydney');
kubectl('get pod', $output, $responseCode);
$output = preg_grep('/mediahq-ingest-vod/', $output);

$dataByStart = [];
$endTimes = [];
foreach ($output as $podLine) {
    list($pod, $junk) = explode(' ', $podLine);
    list($a,$b,$podLabel,$junk) = explode('-', $pod);

    kubectl("logs $pod --tail=15", $output, $responseCode);
    $startLines = preg_grep('/Processing remote file/', $output);
    $endLines = preg_grep('/Added tracking file/', $output);

    foreach ($endLines as $endLine) {
        preg_match('/\[(?<startTime>[^]]+)\] ingest\.INFO: Added tracking file to mark (?<file>[^ ]+)/', $endLine, $matches);
        $endTimes[basename($matches['file'])] = strtotime($matches['startTime']);
    }

    foreach ($startLines as $key=>$line) {
        preg_match('/\[(?<startTime>[^]]+)\] ingest\.INFO: Processing remote file (?<file>[^ ]+)/', $line, $matches);
        $startTime = $matches['startTime'];
        $file = $matches['file'];

        rclone("lsjson s3-antenna-ingest:" . $file, $output, $responseCode);
        $output = json_decode(implode('', $output), true);
        if (empty($output)) {
            rclone("lsjson s3-antenna-ingest:" . str_replace('pending', 'processed', $file), $output, $responseCode);
            $output = json_decode(implode('', $output), true);
        }

        if (empty($output)) {
            //echo "Can't find $file. It probably got moved to another pod\n";
            continue;
        }

        $fileInfo = $output[0];
        $fileInfo['pod'] = $podLabel;
        $dataByStart[strtotime($startTime)] = $fileInfo;
    }
}

ksort($dataByStart);

echo sprintf("| %4s | %-19s | %-50s | %-7s | %-10s |\n", "Pod", "Start time", "File Name", "Size", "Duration");
echo "+------+---------------------+" . str_repeat('-', 52) . "+---------+------------+\n";

foreach ($dataByStart as $startTime => $fileInfo) {
    echo sprintf(
        "| %4s | %19s | %-50s | %7s | %10s |\n",
        $fileInfo['pod'],
        date("Y-m-d H:i:s", $startTime),
        $fileInfo['Name'],
        formatBytes($fileInfo['Size']),
        isset($endTimes[$fileInfo['Name']])
            ? gmdate("H:i:s", ($endTimes[$fileInfo['Name']] - $startTime))
            : '> ' . gmdate("H:i:s", time() - $startTime)
    );
}
echo "\n";

function kubectl(string $cmd, ?array &$output, ?int &$responseCode)
{
    $output = [];
    exec("kubectl --context=antenna-staging {$cmd} 2>&1\n", $output, $responseCode);
}
function rclone(string $cmd, ?array &$output, ?int &$responseCode)
{

    $output = [];
    //echo "rclone {$cmd} 2>&1";
    exec("rclone {$cmd} 2>&1", $output, $responseCode);
}

function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

