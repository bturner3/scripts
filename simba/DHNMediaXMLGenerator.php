<?php
rclone('lsf s3-dhn:switch-content-partner-dhn/', $directories, $responseCode);

$ignoredDirectories = [ '_GFX', '_TestMetadata', 'vod' ];
$allowedExtensions = [ 'mp4', 'mxf', 'mov' ];

foreach ($directories as $dir) {
    if (substr($dir, 0, 1) === '.' || in_array($dir, $ignoredDirectories)) {
        continue;
    }

    rclone("lsjson 's3-dhn:switch-content-partner-dhn/{$dir}' --max-depth=10", $output, $responseCode);
    $files = json_decode(implode('', $output), true);
    if (empty($files)) {
        continue;
    }
    foreach ($files as $file) {
        if ($file['IsDir']) {
            continue;
        }

        $ext = pathinfo($file['Name'], PATHINFO_EXTENSION);
        if (!in_array($ext, $allowedExtensions)) {
            continue;
        }

        $xml = "
<xml>
    <dataSourceName>s3-dhn</dataSourceName>
    <location>switch-content-partner-dhn/{$dir}{$file['Path']}</location>
    <mediaId>" . normalise($file['Name']) . "</mediaId>
</xml>";

        file_put_contents('mediaXML/' . pathInfo($file['Name'], PATHINFO_FILENAME) . '.media.xml', $xml);

        echo "$xml\n\n";
    }
}

function normalise($fileName)
{
    $name = pathinfo($fileName, PATHINFO_FILENAME);

    $normalised = preg_replace('/[^a-zA-Z0-9-_ ]/', '', $name);
    $normalised = preg_replace('/ +/', '-', $normalised);

    return $normalised;
}

function rclone(string $cmd, ?array &$output, ?int &$responseCode)
{
    $output = [];
    #echo "rclone {$cmd} 2>&1\n";
    exec(
        "rclone {$cmd} 2>&1",
        $output,
        $responseCode
    );
}