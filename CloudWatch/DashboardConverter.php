<?php
$dashboardSource = file_get_contents('source');

if ($argc !== 3) {
    echo "Usage: " . $argv[0] . " source-env destination-env\n";
    die();
}
$sourceEnv = $argv[1];
$destEnv = $argv[2];
$envs = [ 'prod', 'sandbox', 'stage' ];

if (!in_array($sourceEnv, $envs) || !in_array($destEnv, $envs)) {
    echo "Both source and destination env must by one of: " . implode(', ', $envs);
    exit(1);
}

$sourceIdx = array_search($sourceEnv, $envs);
$destIdx = array_search($destEnv, $envs);

$replacements = [
    // Prod, Sandbox, Stage

    // FVAU
    [ 'app/freeview-au-ecs-alb/96cacd2393ababa1', 'app/freeview-au-ecs-alb/aa0a631ceeb67759', 'app/freeview-au-ecs-alb/a1ed251a21890065' ],
    [ '', 'app/freeview-au-ecs-alb-catchup/6273c4f1eda7ec78', 'app/freeview-au-ecs-alb-catchup/a2366f4470be37a8' ],
    [ 'targetgroup/freeview-au-api-gateway/13411d9f501e5db9', 'targetgroup/freeview-au-api-gateway/859a81153120b0db', 'targetgroup/freeview-au-api-gateway/deea4a8e5eaaddf3' ],
    [ '', 'targetgroup/freeview-au-catchup-api/bfe485b0ae8426ec', 'targetgroup/freeview-au-catchup-api/d70c04a448c7d345' ],

    // FVNZ

    // CAPI
    [ 'app/core-api-ecs-alb/ebd07a58ce643c21', '', 'app/core-api-ecs-alb/b46fc78ccd49e523' ],
    [ 'prod-capi-20180125043317676600000001', '', 'stage-capi-20171109061649371500000001' ],
    [ '744362489876', '', '948618524387' ], // capi-es client id
    [ 'targetgroup/core-api-capi/4021735bbb075fb0', '', 'targetgroup/core-api-capi/048d9b3c98b03dfe'],
];

echo str_replace(
    array_column($replacements, $sourceIdx),
    array_column($replacements, $destIdx),
    $dashboardSource
);