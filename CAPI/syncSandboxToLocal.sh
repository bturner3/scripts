set -euo pipefail

# Setting up SSH agent
ssh-add -D
ssh-add ~/.ssh/sandbox-core-api.pem

# Connect to sandbox jumpbox and dump DB
echo "Dumping capi DB from sandbox"
ssh -A ec2-user@52.65.203.116 'mysqldump -h db.capi.sandbox -u content_api -p8A1@6rplUaitW9Ld --single-transaction --quick --lock-tables=false content_api | gzip > capi-sandbox.sql.gz'

# Copy to local
echo "Copying dump file to local host"
scp -i ~/.ssh/sandbox-core-api.pem ec2-user@52.65.203.116:~/capi-sandbox.sql.gz .
gunzip capi-sandbox.sql.gz

echo "Importing dump to db container"
cat capi-sandbox.sql | docker exec -i db mysql -u content_api -pcontent_api -D content_api
rm capi-sandbox.sql

echo "Kicking off ES sync"
docker exec -it app php artisan elasticsearch:sync 
