set -euo pipefail

# Setting up SSH agent
ssh-add -D
ssh-add ~/.ssh/prod-core-api.pem

# Connect to prod jumpbox and dump DB
echo "Dumping capi DB from prod"
ssh -A ec2-user@54.206.117.192 'mysqldump -h db.capi.prod -u content_api -psUQpAN8fscNrX --single-transaction --quick --lock-tables=false content_api | gzip > capi-prod.sql.gz'

# Copy to local
echo "Copying dump file to local host"
scp -i ~/.ssh/prod-core-api.pem ec2-user@54.206.117.192:~/capi-prod.sql.gz .
gunzip capi-prod.sql.gz

echo "Importing dump to db container"
cat capi-prod.sql | docker exec -i db mysql -u content_api -pcontent_api -D content_api
rm capi-prod.sql

echo "Kicking off ES sync"
docker exec -it app php artisan elasticsearch:sync 
