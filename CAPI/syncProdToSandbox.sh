# Make interactive - ask what steps to do
# Check for recent dump file on sandbox before copying

# Setting up SSH agent
ssh-add -D
ssh-add ~/.ssh/sandbox-core-api.pem
ssh-add ~/.ssh/prod-core-api.pem

# Connect to prod jumpbox and dump DB
echo "Dumping capi DB from prod"
ssh -A ec2-user@54.206.117.192 'mysqldump -h db.capi.prod -u content_api -psUQpAN8fscNrX --single-transaction --quick --lock-tables=false content_api | gzip > capi-prod.sql.gz'

# Copy to local
echo "Copying dump file to local host"
scp -i ~/.ssh/prod-core-api.pem ec2-user@54.206.117.192:~/capi-prod.sql.gz .

# Send to sandbox
echo "Copying dump file to sandbox"
scp -i ~/.ssh/sandbox-core-api.pem capi-prod.sql.gz ec2-user@52.65.203.116:~
ssh -i ~/.ssh/sandbox-core-api.pem ec2-user@52.65.203.116 gunzip capi-prod.sql.gz

echo "Importing dump file to sandbox"
ssh -A ec2-user@52.65.203.116 'cat capi-prod.sql | mysql -h db.capi.sandbox -u content_api -p8A1@6rplUaitW9Ld -D content_api && rm capi-prod.sql' 

echo "Done. Do this next:"
echo "assume-sandbox && aws s3 sync s3://img-store-prod.switch.tv s3://img-store-sandbox.switch.tv --acl public-read";
echo "Then connect to a capi ECS task and sync ES";
# List CAPI instances
#CAPI_INSTANCES=`AWS_PROFILE=switch-sandbox aws ec2 describe-instances --filter Name=instance.group-name,Values=core-api-ecs-instance | jq '.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress'  | sed 's/"//g'`

# Loop through CAPI instances looking for one that has active tasks (normally the first... but can't be sure at all times) 
#for CAPI_IP in $CAPI_INSTANCES; do
#    echo "Checking $CAPI_IP for active capi tasks"
#    ssh -A ec2-user@52.65.203.116 ssh -A $CAPI_IP echo $CAPI_IP = `hostname`
#done;
