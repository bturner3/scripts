<?php
// Pluck -y out of any arg and if set suppress interaction
$force = false;
if (in_array('-y', $argv)) {
    $force = true;
    unset($argv[array_search('-y', $argv)]);
    $argv = array_values($argv);
}

// get env
if (!isset($argv[1])) {
    echo "no env specified\n";
    exit(1);
}
$env = $argv[1];

try {
    (new InaccessibleShowCleanup($env, $force))->run();
} catch (Exception $e) {
    echo "ERROR: " . $e->getMessage() . "\n";
}

class InaccessibleShowCleanup
{
    protected $realms = [
        // Stage
        'c064763a-2c55-4533-9509-4c1cbaad1c69' => [
            'name' => 'FVAU',
            'host' => 'http://fvau-api-%s.switch.tv'
        ],
        // Sandbox
        '905425c9-538f-4e36-939f-a46a60ea6dfa' => [
            'name' => 'FVAU',
            'host' => 'http://fvau-api-%s.switch.tv'
        ],

        // FVNZ is the same on stage/sandbox
        'debbe4e4-7677-475d-a23d-a5524f0798e6' => [
            'name' => 'FVNZ',
            'host' => 'http://fvnz-capi-%s.switch.tv'
        ],

        // PROD
        'f2d729ef-ffa5-4a7e-90d8-74cdb4e119d7' => [
            'name' => 'FVAU',
            'host' => 'http://fvau-api-%s.switch.tv'
        ],
        '2e0a53b8-0194-11e8-ba89-0ed5f89f718b' => [
            'name' => 'FVNZ',
            'host' => 'http://fvnz-capi-%s.switch.tv'
        ]
    ];

    protected $dry = true;
    protected $env;

    /**
     * InaccessibleShowCleanup constructor.
     *
     * @param string $env
     * @param bool $force
     * @throws Exception
     */
    public function __construct($env, $force = false)
    {
        if (!in_array($env, [ 'stage', 'sandbox', 'prod' ])) {
            throw new Exception("Unknown environment '$env'");
        }

        $this->env = $env;

        if ($env === 'prod' && !$force) {
            echo "Prod? You sure? This will delete many things... although they should be safe things.. but still.. be sure\n> ";
            $handle = fopen("php://stdin", "r");
            $line = fgets($handle);
            if (trim($line) != 'yes') {
                echo "well well well aren't we lucky I asked!\n";
                exit;
            }
            fclose($handle);
            echo "OK THEN! LETS GO!\n";
        }
    }

    public function run()
    {
        //$this->processFVNZ();
        $this->processFVAU();
    }

    protected function processFVNZ()
    {
        $host = "http://fvnz-capi-{$this->env}.switch.tv";
        $limit = 100;

        $broadcasters = [ 'fvnz', 'maoritv', 'mediaworks', 'rhemamedia', 'tvnz' ];
        foreach ($broadcasters as $broadcaster) {
            echo "Loading FVNZ shows for broadcaster: $broadcaster\n";

            $offset = 0;
            $allShows = [];
            $auth = getenv('FVNZ_' . strtoupper($broadcaster) . '_AUTH_' . strtoupper($this->env));

            do {
                $uri = "$host/content/v1/shows?on_air=exclude&on_demand=exclude&limit=$limit&offset=$offset&include_related=1&related_levels=2";
                $shows = $this->get($uri, $auth);

                $allShows = array_merge($allShows, $shows);
                $offset += $limit;
            } while (!empty($shows));
            echo "Loaded " . count($allShows) . " shows\n";

            foreach ($allShows as $show) {
                $this->processShow($host, $show, $auth);
            }
        }
    }

    protected function processFVAU()
    {
        $host = "http://fvau-api-{$this->env}.switch.tv";
        $limit = 100;

        echo "Loading FVAU shows";

        $offset = 0;
        $allShows = [];
        $auth = getenv('FVAU_' . '_AUTH_' . strtoupper($this->env));

        do {
            $uri = "$host/content/v1/shows?on_air=exclude&on_demand=exclude&limit=$limit&offset=$offset&include_related=1&related_levels=2";
            echo $uri . "\n";
            $shows = $this->get($uri);

            $allShows = array_merge($allShows, $shows);
            $offset += $limit;
        } while (!empty($shows));
        echo "Loaded " . count($allShows) . " shows\n";

        foreach ($allShows as $show) {
            $this->processShow($host, $show, $auth);
        }
    }

    protected function processShow($host, $show, $auth = null)
    {
        // Delete the show itself
        $showId = $show['id'];
        echo "Deleting show: $showId - {$show['title']}\n";
        $this->delete("$host/content/v1/shows/$showId", $auth);

        // Delete any related seasons that have no other related shows (ie anything we just orphaned)
        $seasons = $show['related']['seasons'] ?? [];
        foreach ($seasons as $season) {
            $relatedShows = $season['related']['shows'];
            if (count($relatedShows) !== 1 || $relatedShows[0]['id'] !== $showId) {
                continue;
            }

            $seasonId = $season['id'];
            echo "\tDeleting orphaned season: $seasonId\n";
            $this->delete("$host/content/v1/seasons/$seasonId", $auth);

            // And similarly now delete any episodes that we have just orphaned (ie no other related seasons)
            $episodes = $season['related']['episodes'] ?? [];
            foreach ($episodes as $episode) {
                // We have max related levels set to 2, so we need a new call to fetch episode related
                $episodeId = $episode['id'];
                $episode = $this->get("$host/content/v1/episodes/$episodeId?include_related=1");

                $relatedShows = $episode['related']['shows'] ?? [];
                $relatedSeasons = $episode['related']['seasons'] ?? [];

                // Multiple related shows, or related to single show that we didn't just delete
                if (count($relatedShows) > 1 || (count($relatedShows) === 1 && $relatedShows[0]['id'] !== $showId)) {
                    continue;
                }

                // Related to any season other than the one we just deleted
                if (count($relatedSeasons) !== 1 || $relatedSeasons[0]['id'] !== $seasonId) {
                    continue;
                }

                // Otherwise let it die
                echo "\t\tDeleting orphaned episode: $episodeId\n";
                $this->delete("$host/content/v1/episodes/$episodeId", $auth);
            }
        }
    }

    /**
     * Get the related entities of the specified image
     *
     * @param string $uri
     * @param string $auth
     * @return array
     * @throws Exception
     */
    protected function get($uri, $auth = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($auth) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [ "Authorization: Bearer $auth" ]);
        }

        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status === 404 || $status === 204) {
            return [];
        }
        if ($status !== 200) {
            throw new Exception("Unable to fetch -- $status: $result");
        }

        return json_decode($result, true)['data'];
    }

    /**
     * Call the relevant gateway to delete the specified image
     *
     * @return void
     */
    public function delete($uri, $auth)
    {
        if ($this->dry) {
            return;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [ "Authorization: Bearer $auth" ]);
        curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status !== 200) {
            echo "ERROR: Unable to delete -- $status";
        }
    }
}

class NotFoundException extends Exception {}
